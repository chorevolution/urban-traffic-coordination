package eu.chorevolution.urbantrafficcoordination.seada.services.dtshere.business;

import eu.chorevolution.urbantrafficcoordination.seada.services.dtshere.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtshere.RoutesSuggestion;


public interface DTSHereService {

	RoutesSuggestion getRoutes(RoutesRequest routesRequest) throws BusinessException;
	
}
