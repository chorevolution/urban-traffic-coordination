var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var request = require('request');

var portNumber = 3001;
var app = express()
app.use(bodyParser.json());

app.post('/bridgeNextClosure', function(req, res) {
    // takes bridgeId, example of post request body {"date" : "1475746321493", "nextTrafficLightId":"28"} 

    request({
        url: 'http://data.goteborg.se/BridgeService/v1.0/GetGABOpenedStatus/152d47b0-754b-4718-a635-6a7db1374572?format=json',
        method: 'GET'
    }, function(error, response, body) {
        var isBridgeClosed = JSON.parse(response.body).Value;
        var currentTime = new Date();
        var nextOpenedTime;
        var nextClosureFrom;
        var nextClosureTo;

        if (isBridgeClosed) {
            nextOpenedTime = currentTime.getTime() + 7 * 60000; // we estimate that the bridge is closed for 7 minutes more in case that it is already closed
        } else {
            nextOpenedTime = currentTime.getTime(); //+1-1 is transformation to int
        }

        var currentHour = Math.floor(currentTime.getTime() / 3600000) % 24 + 2; //hour of the day, +1 because of timezone adjustment
        var currentMinute = Math.floor(currentTime.getTime() / 60000) % 60; //minute of the hour
        var currentDayInMillis = Math.floor(currentTime.getTime() / (3600000 * 24)) * (3600000 * 24);

        if ((currentHour >= 9) && (currentHour < 18)) {
            nextClosureFrom = currentDayInMillis + (3600000 * 18);
            nextClosureTo = currentDayInMillis + 3600000 * 18 + 60000 * 10; // closure time from 18.00 to 18.10
        }

        if ((currentHour >= 0) && (currentHour < 9)) {
            nextClosureFrom = currentDayInMillis + (3600000 * 9);
            nextClosureTo = currentDayInMillis + 3600000 * 9 + 60000 * 10; // closure time from 9.00 to 9.10
        }

        if ((currentHour >= 18) && (currentHour < 24)) {
            nextClosureFrom = currentDayInMillis + (3600000 * (24 + 9));
            nextClosureTo = currentDayInMillis + (3600000 * (24 + 9)) + 60000 * 10; // closure time from 9.00 to 9.10 next day
        }
        console.log(nextClosureFrom);
        res.json({
            bridgeStatusInformationResponse: {
                closureStatus: { isClosed: isBridgeClosed, opensAtTimeMillis: nextOpenedTime },
                nextClosure: { fromMillis: nextClosureFrom, toMillis: nextClosureTo }
            }
        });
    });
});

app.listen(portNumber);
console.log('server is listening on port ' + portNumber);
