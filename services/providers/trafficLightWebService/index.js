var express = require('express');
var bodyParser = require('body-parser');

var portNumber = 3000;
var app = express()
app.use(bodyParser.json());

app.post('/trafficLightColor', function(req, res) { 
// takes time in milliseconds, example of post request body {"date" : "1475746321493", "nextTrafficLightId":"28"} 
	var date = req.body.date;
	if (isOdd(Math.floor(parseInt(date)/60000))){		
  		res.json({color: "green"});
	}else{
		res.json({color: "red"});
	}
})
 
app.listen(portNumber);
console.log('server is listening on port ' + portNumber);

function isOdd(num) { return num % 2;}