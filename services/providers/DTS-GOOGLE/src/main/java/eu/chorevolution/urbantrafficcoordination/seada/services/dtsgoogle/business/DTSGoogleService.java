package eu.chorevolution.urbantrafficcoordination.seada.services.dtsgoogle.business;

import eu.chorevolution.urbantrafficcoordination.seada.services.dtsgoogle.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsgoogle.RoutesSuggestion;



public interface DTSGoogleService {

	RoutesSuggestion getRoutes(RoutesRequest routesRequest) throws BusinessException;
}
