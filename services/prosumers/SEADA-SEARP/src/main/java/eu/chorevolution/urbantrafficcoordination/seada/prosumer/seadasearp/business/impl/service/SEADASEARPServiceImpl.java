package eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.business.impl.service;

import java.util.List;

import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.EcoRoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.EcoRoutesResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.EcoSpeedRoutesInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.EcoSpeedRoutesInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.Route;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.RoutesSuggestion;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.business.ChoreographyInstanceMessages;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.business.SEADASEARPService;
/*import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRouteInformation;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRouteSegmentInformation;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.RouteSegment;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.TrafficRouteInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.TrafficRouteInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.TrafficRouteSegmentInformation;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.Waypoint;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.WaypointInfo;*/

@Service
public class SEADASEARPServiceImpl implements SEADASEARPService{

	@Override
	public RoutesRequest createRoutesRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
		
		RoutesRequest routesRequest = new RoutesRequest();
		System.out.println("reached createRoutesRequest request");
		EcoRoutesRequest ecoRoutesRequest = (EcoRoutesRequest) choreographyInstanceMessages.getMessage("EcoRoutesRequest");
		routesRequest.setDestination(ecoRoutesRequest.getDestination());
		routesRequest.setOrigin(ecoRoutesRequest.getOrigin());				
		return routesRequest;
		/**
		*	TODO write the code that generates EcoRoutesResponse message 
		*	that has to be sent to receiverParticipantName within the interaction belonging to choreographyTaskName
		*	You can exploit the following API from choreographyInstanceMessages
		*	1. Get a Message using its name 
		*		- example: 
		*			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessage("EcoRoutesRequest");
		*		A null value is returned if no message has been found.
		*	2. Get a list of messages using a message name
		*		- example: 
		*			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessages("EcoRoutesRequest");
		*		A null value is returned if no message has been found. 
		*	3. Get a message using its name and the name of the sender participant
		*		- example: 
		*			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND");
		*		A null value is returned if no message has been found. 
		*	4. Get a list of messages using a message name and a sender participant name
		*		- example: 
		*			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND"); 
		*		A null value is returned if no message has been found.
		*	5. Get a message using its name, the name of the sender participant and the name of the task in which 
		*		the message has been exchanged
		*		- example: 
		*			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");	
		*		A null value is returned if no message has been found.
		*	6. Get a list of messages using a message name, the name of the sender participant and the name of the task in which 
		*		the messages has been exchanged
		*		- example: 
		*			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");
		*		A null value is returned if no message has been found.
		*	7. Get a message using its name and the name of the receiver participant
		*		- example: 
		*			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedParticipant("RoutesRequest", "DTS-GOOGLE");
		*		A null value is returned if no message has been found. 
		*	8. Get a message using its name, the name of the receiver participant and the name of the task in which 
		*		the message has been exchanged
		*		- example: 
		*			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");;	
		*		A null value is returned if no message has been found.
		*	9. Get a list of messages using a message name and a receiver participant name
		*		- example: 
        *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE");
		*	10. Get a list of messages using a message name, the name of the receiver participant and the name of the task in which 
		*		the messages has been exchanged
		*		- example: 
		*			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");
		*		A null value is returned if no message has been found.
		*/			
	}

	@Override
	public EcoSpeedRoutesInformationRequest createEcoSpeedRoutesInformationRequest(
			ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, 
			String receiverParticipantName) {
		
		System.out.println("reached createEcoSpeedRoutesInformationRequest");
		
		EcoSpeedRoutesInformationRequest ecoSpeedRoutesInformationRequest = new EcoSpeedRoutesInformationRequest();
		
		List<RoutesSuggestion> routesSuggestions = choreographyInstanceMessages.getMessages("RoutesSuggestion");
		//List<Route> routes = ecoSpeedRoutesInformationRequest.getRoutes();
		
		for (RoutesSuggestion routeSuggestion : routesSuggestions) {
			ecoSpeedRoutesInformationRequest.getRoutes().addAll(routeSuggestion.getRoutes());
			System.out.println("routes quatity in ecospeed routes suggestionReq : " + ecoSpeedRoutesInformationRequest.getRoutes().size());
		}
		System.out.println("ECO SPEED ROUTES INFORMATION REQUEST WITH ROUTES NUMBER: "+ecoSpeedRoutesInformationRequest.getRoutes().size());
		return ecoSpeedRoutesInformationRequest; //here request contains two routes, which came from google and here!!!!!!!!!!! So far so good!
		/**
		*	TODO write the code that generates EcoRoutesResponse message
		*	that has to be sent to receiverParticipantName within the interaction belonging to choreographyTaskName
		*	You can exploit the following API from choreographyInstanceMessages
		*	1. Get a Message using its name 
		*		- example: 
		*			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessage("EcoRoutesRequest");
		*		A null value is returned if no message has been found.
		*	2. Get a list of messages using a message name
		*		- example: 
		*			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessages("EcoRoutesRequest");
		*		A null value is returned if no message has been found. 
		*	3. Get a message using its name and the name of the sender participant
		*		- example: 
		*			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND");
		*		A null value is returned if no message has been found. 
		*	4. Get a list of messages using a message name and a sender participant name
		*		- example: 
		*			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND"); 
		*		A null value is returned if no message has been found.
		*	5. Get a message using its name, the name of the sender participant and the name of the task in which 
		*		the message has been exchanged
		*		- example: 
		*			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");	
		*		A null value is returned if no message has been found.
		*	6. Get a list of messages using a message name, the name of the sender participant and the name of the task in which 
		*		the messages has been exchanged
		*		- example: 
		*			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");
		*		A null value is returned if no message has been found.
		*	7. Get a message using its name and the name of the receiver participant
		*		- example: 
		*			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedParticipant("RoutesRequest", "DTS-GOOGLE");
		*		A null value is returned if no message has been found. 
		*	8. Get a message using its name, the name of the receiver participant and the name of the task in which 
		*		the message has been exchanged
		*		- example: 
		*			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");;	
		*		A null value is returned if no message has been found.
		*	9. Get a list of messages using a message name and a receiver participant name
		*		- example: 
        *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE");
		*	10. Get a list of messages using a message name, the name of the receiver participant and the name of the task in which 
		*		the messages has been exchanged
		*		- example: 
		*			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");
		*		A null value is returned if no message has been found.
		*/		
	}

	@Override
	public EcoRoutesResponse createEcoRoutesResponse(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {

		EcoRoutesResponse ecoRoutesResponse = new EcoRoutesResponse();
		System.out.println("reached createEcoRoutesResponse");
		
		EcoSpeedRoutesInformationResponse ecoRoutesInfoResponse = (EcoSpeedRoutesInformationResponse) choreographyInstanceMessages.getMessage("EcoSpeedRoutesInformationResponse");
		System.out.println("EcoSpeedRoutesInformationResponse collection size: " + ecoRoutesInfoResponse.getEcoSpeedRoutesInformation().size());
		ecoRoutesResponse.getEcoSpeedRoutesInformation().addAll(ecoRoutesInfoResponse.getEcoSpeedRoutesInformation());
		//System.out.println("CREATES ECO SPEED ROUTES INFORMATION RESPONSE WITH ROUTES NUMBER: "+ecoRoutesInfoResponse.getEcoSpeedRoutesInformation().size());
		return ecoRoutesResponse;
		/**
		*	TODO write the code that generates EcoRoutesResponse message
		*	that has to be sent to receiverParticipantName within the interaction belonging to choreographyTaskName
		*	You can exploit the following API from choreographyInstanceMessages
		*	1. Get a Message using its name 
		*		- example: 
		*			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessage("EcoRoutesRequest");
		*		A null value is returned if no message has been found.
		*	2. Get a list of messages using a message name
		*		- example: 
		*			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessages("EcoRoutesRequest");
		*		A null value is returned if no message has been found. 
		*	3. Get a message using its name and the name of the sender participant
		*		- example: 
		*			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND");
		*		A null value is returned if no message has been found. 
		*	4. Get a list of messages using a message name and a sender participant name
		*		- example: 
		*			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND"); 
		*		A null value is returned if no message has been found.
		*	5. Get a message using its name, the name of the sender participant and the name of the task in which 
		*		the message has been exchanged
		*		- example: 
		*			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");	
		*		A null value is returned if no message has been found.
		*	6. Get a list of messages using a message name, the name of the sender participant and the name of the task in which 
		*		the messages has been exchanged
		*		- example: 
		*			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");
		*		A null value is returned if no message has been found.
		*	7. Get a message using its name and the name of the receiver participant
		*		- example: 
		*			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedParticipant("RoutesRequest", "DTS-GOOGLE");
		*		A null value is returned if no message has been found. 
		*	8. Get a message using its name, the name of the receiver participant and the name of the task in which 
		*		the message has been exchanged
		*		- example: 
		*			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");;	
		*		A null value is returned if no message has been found.
		*	9. Get a list of messages using a message name and a receiver participant name
		*		- example: 
        *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE");
		*	10. Get a list of messages using a message name, the name of the receiver participant and the name of the task in which 
		*		the messages has been exchanged
		*		- example: 
		*			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");
		*		A null value is returned if no message has been found.
		*/		
	}

	@Override
	public void setEcoSpeedRoutesInformation(EcoSpeedRoutesInformationResponse ecoSpeedRoutesInformationResponse, String senderParticipantName) {

		/**
		*	TODO Add your business logic upon the reception of EcoSpeedRoutesInformationResponse message from senderParticipantName
		*/		
	}

	@Override
	public void receiveRoutesSuggestion(RoutesSuggestion routesSuggestion, String choreographyTaskName, String senderParticipantName) {
		
		/**
		*	TODO Add your business logic upon the reception of RoutesSuggestion message from senderParticipantName 
		*		 within the interaction belonging to choreographyTaskName
		*/	
	}	
	
	@Override
	public void getEcoRoutes(EcoRoutesRequest ecoRoutesRequest, String senderParticipantName) {

		/**
		*	TODO Add your business logic upon the reception of EcoRoutesRequest message from senderParticipantName
		*/			
	}



}
