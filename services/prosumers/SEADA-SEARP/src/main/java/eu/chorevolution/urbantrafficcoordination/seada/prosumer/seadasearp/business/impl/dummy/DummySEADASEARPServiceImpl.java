package eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.EcoRoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.EcoRoutesResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.EcoSpeedRouteInformation;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.EcoSpeedRouteSegmentInformation;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.EcoSpeedRoutesInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.EcoSpeedRoutesInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.Route;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.RoutesSuggestion;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.Waypoint;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.business.ChoreographyInstanceMessages;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.business.SEADASEARPService;

@Service
public class DummySEADASEARPServiceImpl implements SEADASEARPService{

	private static Logger logger = LoggerFactory.getLogger(DummySEADASEARPServiceImpl.class);
	
	@Override
	public void setEcoSpeedRoutesInformation(EcoSpeedRoutesInformationResponse ecoSpeedRoutesInformationResponse, String senderParticipantName) {
		
		logger.info("CALLED setEcoSpeedRoutesInformation BUSINESS-LOGIC ON SEADA-SEARP");	
	}

	@Override
	public void getEcoRoutes(EcoRoutesRequest ecoRoutesRequest, String senderParticipantName) {
		
		logger.info("CALLED getEcoRoutes BUSINESS-LOGIC ON SEADA-SEARP");
	}

	@Override
	public RoutesRequest createRoutesRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
		
		logger.info("CALLED createRoutesRequest BUSINESS-LOGIC ON SEADA-SEARP");
		RoutesRequest routesRequest = new RoutesRequest();
		Waypoint waypoint = new Waypoint();
		waypoint.setLat(11);
		waypoint.setLon(22);
		routesRequest.setOrigin(waypoint);
		return routesRequest;
	}

	@Override
	public EcoSpeedRoutesInformationRequest createEcoSpeedRoutesInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
		
		logger.info("CALLED createEcoSpeedRoutesInformationRequest BUSINESS-LOGIC ON SEADA-SEARP");
		EcoSpeedRoutesInformationRequest ecoSpeedRoutesInformationRequest = new EcoSpeedRoutesInformationRequest();
		Route route1 = new Route();
		route1.setId(1);
		Route route2 = new Route();
		route2.setId(2);	
		Route route3 = new Route();
		route3.setId(3);			
		ecoSpeedRoutesInformationRequest.getRoutes().add(route1);
		ecoSpeedRoutesInformationRequest.getRoutes().add(route2);
		ecoSpeedRoutesInformationRequest.getRoutes().add(route3);		
		return ecoSpeedRoutesInformationRequest;
	}

	@Override
	public void receiveRoutesSuggestion(RoutesSuggestion routesSuggestion, String choreographyTaskName, String senderParticipantName) {
		
		logger.info("CALLED receiveRoutesSuggestion BUSINESS-LOGIC ON SEADA-SEARP");
	}

	@Override
	public EcoRoutesResponse createEcoRoutesResponse(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
		
		logger.info("CALLED createEcoRoutesResponse BUSINESS-LOGIC ON SEADA-SEARP");
		EcoRoutesResponse ecoRoutesResponse = new EcoRoutesResponse();
		EcoSpeedRouteInformation ecoSpeedRouteInformation = new EcoSpeedRouteInformation();
		EcoSpeedRouteSegmentInformation ecoSpeedRouteSegmentInformation = new EcoSpeedRouteSegmentInformation();
		ecoSpeedRouteInformation.getEcoSpeedRouteSegmentsInformation().add(ecoSpeedRouteSegmentInformation);
		ecoRoutesResponse.getEcoSpeedRoutesInformation().add(ecoSpeedRouteInformation);
		return ecoRoutesResponse;
	}
}	
