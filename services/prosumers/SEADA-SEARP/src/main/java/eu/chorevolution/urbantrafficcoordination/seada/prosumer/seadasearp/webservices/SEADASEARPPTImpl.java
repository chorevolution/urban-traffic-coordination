package eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.EcoRoutesRequestReturnType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.EcoRoutesRequestType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.EcoRoutesResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.EcoSpeedRoutesInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.ReceiveRoutesSuggestionReturnType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.ReceiveRoutesSuggestionType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.SEADASEARPPT;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.SendRequestType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.SetEcoSpeedRoutesInformationReturnType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.SetEcoSpeedRoutesInformationType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.business.ChoreographyDataService;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.business.ChoreographyInstanceMessagesStore;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.business.SEADASEARPService;

@Component(value="SEADASEARPPTImpl")
public class SEADASEARPPTImpl implements SEADASEARPPT{

	@Autowired
	private SEADASEARPService service;
	@Autowired
	private ChoreographyDataService choreographyDataService;
	
	private static Logger logger = LoggerFactory.getLogger(SEADASEARPPTImpl.class);
	
	@Override
	public SetEcoSpeedRoutesInformationReturnType setEcoSpeedRoutesInformation(SetEcoSpeedRoutesInformationType parameters) {

		logger.info("CALLED setEcoSpeedRoutesInformation ON SEADA-SEARP");
		logger.info("START TIME OPERATION setEcoSpeedRoutesInformation ON SEADA-SEARP: "+System.currentTimeMillis());				
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), parameters.getMessageData());
		service.setEcoSpeedRoutesInformation(parameters.getMessageData(), parameters.getSenderParticipantName());
		logger.info("END TIME OPERATION setEcoSpeedRoutesInformation ON SEADA-SEARP: "+System.currentTimeMillis());			
		return new SetEcoSpeedRoutesInformationReturnType();
	}

	@Override
	public EcoRoutesRequestReturnType getEcoRoutes(EcoRoutesRequestType parameters) {

		logger.info("CALLED getEcoRoutes ON SEADA-SEARP");		
		logger.info("START TIME OPERATION getEcoRoutes ON SEADA-SEARP: "+System.currentTimeMillis());	
		logger.info(String.valueOf(parameters.getMessageData().getOrigin().getLon()));		
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), parameters.getMessageData());	
		service.getEcoRoutes(parameters.getMessageData(), parameters.getSenderParticipantName());
		logger.info("END TIME OPERATION getEcoRoutes ON SEADA-SEARP: "+System.currentTimeMillis());
		return new EcoRoutesRequestReturnType();
	}

	@Override
	public ReceiveRoutesSuggestionReturnType receiveRoutesSuggestion(ReceiveRoutesSuggestionType parameters) {

		logger.info("CALLED receiveRoutesSuggestion ON SEADA-SEARP");	
		logger.info("START TIME OPERATION receiveRoutesSuggestion ON SEADA-SEARP: "+System.currentTimeMillis());
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), parameters.getMessageData());
		service.receiveRoutesSuggestion(parameters.getMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		logger.info("END TIME OPERATION receiveRoutesSuggestion ON SEADA-SEARP: "+System.currentTimeMillis());
		return new ReceiveRoutesSuggestionReturnType();
	}

	@Override
	public EcoRoutesResponse sendEcoRoutesResponse(SendRequestType parameters) {
		
		logger.info("CALLED sendEcoRoutesResponse ON SEADA-SEARP");
		logger.info("START TIME OPERATION sendEcoRoutesResponse ON SEADA-SEARP: "+System.currentTimeMillis());			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		EcoRoutesResponse ecoRoutesResponse = service.createEcoRoutesResponse(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), ecoRoutesResponse);			
		logger.info("END TIME OPERATION sendEcoRoutesResponse ON SEADA-SEARP: "+System.currentTimeMillis());	
		return ecoRoutesResponse;
	}

	@Override
	public RoutesRequest sendRoutesRequest(SendRequestType parameters) {
		
		logger.info("CALLED sendRoutesRequest ON SEADA-SEARP");	
		logger.info("START TIME OPERATION sendRoutesRequest ON SEADA-SEARP: "+System.currentTimeMillis());
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		RoutesRequest routesRequest = service.createRoutesRequest(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), routesRequest);
		logger.info("END TIME OPERATION sendRoutesRequest ON SEADA-SEARP: "+System.currentTimeMillis());
		return routesRequest;
	}

	@Override
	public EcoSpeedRoutesInformationRequest sendEcoSpeedRoutesInformationRequest(SendRequestType parameters) {

		logger.info("CALLED sendEcoSpeedRoutesInformationRequest ON SEADA-SEARP");
		logger.info("START TIME OPERATION sendEcoSpeedRoutesInformationRequest ON SEADA-SEARP: "+System.currentTimeMillis());
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		EcoSpeedRoutesInformationRequest ecoSpeedRoutesInformationRequest = service.createEcoSpeedRoutesInformationRequest(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), ecoSpeedRoutesInformationRequest);		
		logger.info("END TIME OPERATION sendEcoSpeedRoutesInformationRequest ON SEADA-SEARP: "+System.currentTimeMillis());
		return ecoSpeedRoutesInformationRequest;
	}	
	
}
