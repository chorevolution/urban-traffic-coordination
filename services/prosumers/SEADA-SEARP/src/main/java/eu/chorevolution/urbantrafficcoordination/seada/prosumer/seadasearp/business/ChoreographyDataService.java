package eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadasearp.business;

public interface ChoreographyDataService {

	ChoreographyInstanceMessagesStore getChoreographyInstanceMessages(String choreographyID);
	
}
