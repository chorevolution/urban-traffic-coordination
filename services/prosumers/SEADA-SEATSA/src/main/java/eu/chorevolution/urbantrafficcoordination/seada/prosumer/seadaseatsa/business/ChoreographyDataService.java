package eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.business;

public interface ChoreographyDataService {

	ChoreographyInstanceMessagesStore getChoreographyInstanceMessages(String choreographyID);
	
}
