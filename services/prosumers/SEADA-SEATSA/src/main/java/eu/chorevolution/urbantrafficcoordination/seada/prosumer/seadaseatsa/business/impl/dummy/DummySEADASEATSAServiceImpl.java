package eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRouteInformation;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRouteInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRouteInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRouteSegmentInformation;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRoutesInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRoutesInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.RouteSegment;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.TrafficRouteInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.TrafficRouteInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.Waypoint;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.business.ChoreographyInstanceMessages;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.business.SEADASEATSAService;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.model.ChoreographyLoopIndexes;

@Service
public class DummySEADASEATSAServiceImpl implements SEADASEATSAService{

	private static Logger logger = LoggerFactory.getLogger(DummySEADASEATSAServiceImpl.class);
	
	@Override
	public EcoSpeedRoutesInformationResponse createEcoSpeedRoutesInformationResponse(
			ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {

		logger.info("CALLED createEcoSpeedRoutesInformationResponse BUSINESS-LOGIC ON SEADA-SEATSA");
		EcoSpeedRoutesInformationResponse ecoSpeedRoutesInformationResponse = new EcoSpeedRoutesInformationResponse();
		EcoSpeedRouteInformation ecoSpeedRouteInformation = new EcoSpeedRouteInformation();
		EcoSpeedRouteSegmentInformation ecoSpeedRouteSegmentInformation = new EcoSpeedRouteSegmentInformation();
		ecoSpeedRouteInformation.getEcoSpeedRouteSegmentsInformation().add(ecoSpeedRouteSegmentInformation);
		ecoSpeedRoutesInformationResponse.getEcoSpeedRoutesInformation().add(ecoSpeedRouteInformation);
		return ecoSpeedRoutesInformationResponse;
	}

	@Override
	public TrafficRouteInformationRequest createTrafficRouteInformationRequest(
			ChoreographyInstanceMessages choreographyInstanceMessages, ChoreographyLoopIndexes choreographyLoopIndexes, String choreographyTaskName, String receiverParticipantName) {

		logger.info("CALLED createTrafficRouteInformationRequest BUSINESS-LOGIC ON SEADA-SEATSA");
		// choreographyLoopIndexes.getChoreographyLoopIndex("EcoFriendlyRouteInformationCollection");
		TrafficRouteInformationRequest trafficRouteInformationRequest = new TrafficRouteInformationRequest();
		RouteSegment routeSegment1 = new RouteSegment();
		Waypoint waypoint1 = new Waypoint();
		waypoint1.setLat(11);
		waypoint1.setLon(11);		
		routeSegment1.setStart(waypoint1);
		RouteSegment routeSegment2 = new RouteSegment();
		Waypoint waypoint2 = new Waypoint();
		waypoint2.setLat(22);
		waypoint2.setLon(22);		
		routeSegment2.setStart(waypoint2);		
		RouteSegment routeSegment3 = new RouteSegment();
		Waypoint waypoint3 = new Waypoint();
		waypoint3.setLat(33);
		waypoint3.setLon(33);		
		routeSegment3.setStart(waypoint3);		
		trafficRouteInformationRequest.getRouteSegments().add(routeSegment1);
		trafficRouteInformationRequest.getRouteSegments().add(routeSegment2);
		trafficRouteInformationRequest.getRouteSegments().add(routeSegment3);
		return trafficRouteInformationRequest;
	}

	@Override
	public void getEcoSpeedRoutesInformation(EcoSpeedRoutesInformationRequest ecoSpeedRoutesInformationRequest, String senderParticipantName) {
		
		logger.info("CALLED getEcoSpeedRoutesInformation BUSINESS-LOGIC ON SEADA-SEATSA");
	}

	@Override
	public void setRouteTrafficInformation(TrafficRouteInformationResponse trafficRouteInformationResponse, String senderParticipantName) {
		
		logger.info("CALLED setRouteTrafficInformation BUSINESS-LOGIC ON SEADA-SEATSA");
	}

	@Override
	public void getEcoSpeedRouteInformation(EcoSpeedRouteInformationRequest ecoSpeedRouteInformationRequest, String senderParticipantName) {
		
		logger.info("CALLED getEcoSpeedRouteInformation BUSINESS-LOGIC ON SEADA-SEATSA");
	}

	@Override
	public EcoSpeedRouteInformationResponse createEcoSpeedRouteInformationResponse(
			ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
		
		logger.info("CALLED createEcoSpeedRouteInformationResponse BUSINESS-LOGIC ON SEADA-SEATSA");
		EcoSpeedRouteInformationResponse ecoSpeedRouteInformationResponse = new EcoSpeedRouteInformationResponse();
		EcoSpeedRouteInformation ecoSpeedRouteInformation = new EcoSpeedRouteInformation();
		ecoSpeedRouteInformationResponse.setEcoSpeedRouteInformation(ecoSpeedRouteInformation);
		return ecoSpeedRouteInformationResponse;
	}

}
