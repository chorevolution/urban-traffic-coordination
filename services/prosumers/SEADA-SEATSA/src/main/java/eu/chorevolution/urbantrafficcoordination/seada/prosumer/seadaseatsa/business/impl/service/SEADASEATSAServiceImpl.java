package eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.business.impl.service;

import java.util.List;

import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRouteInformation;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRouteInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRouteInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRouteSegmentInformation;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRoutesInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRoutesInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.ExtraDataWaypoint;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.ExtraDataWaypoints;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.RouteSegment;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.TrafficRouteInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.TrafficRouteInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.TrafficRouteSegmentInformation;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.Waypoint;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.WaypointInfo;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.business.ChoreographyInstanceMessages;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.business.SEADASEATSAService;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.model.ChoreographyLoopIndexes;

@Service
public class SEADASEATSAServiceImpl implements SEADASEATSAService{

	@Override
	public EcoSpeedRoutesInformationResponse createEcoSpeedRoutesInformationResponse(
			ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
		
		System.out.println("reached SEADASEATSAServiceImpl.createEcoSpeedRoutesInformationResponse");
		
		EcoSpeedRoutesInformationResponse ecoSpeedRoutesInformationResponse = new EcoSpeedRoutesInformationResponse();		
		
		List<TrafficRouteInformationResponse> trafficRouteInformationResponses = choreographyInstanceMessages.getMessages("TrafficRouteInformationResponse");
		System.out.println("TrafficRouteInformationResponse array size: " + trafficRouteInformationResponses.size());
		for(TrafficRouteInformationResponse trafficRouteInfo : trafficRouteInformationResponses){
			EcoSpeedRouteInformation ecoSpeedRouteInformation = new EcoSpeedRouteInformation();
			for(TrafficRouteSegmentInformation segment : trafficRouteInfo.getTrafficRouteInformation()){
				EcoSpeedRouteSegmentInformation ecoSpeedRouteSegmentInformation = new EcoSpeedRouteSegmentInformation();
				ecoSpeedRouteSegmentInformation.setCongestionLevel(segment.getCongestionLevel());
				ecoSpeedRouteSegmentInformation.setEcoLevel(1);//we use this constant value here since the way of calculation for this value is not ready yet. In the future this value is supposed to depend on all parameters we receive from providers
				
				ExtraDataWaypoints dataWaypoints = new ExtraDataWaypoints();//Part of a dirty hack, getting the speed through a specific value of the waypoint
				int ecoSpeed = 0;
				for (ExtraDataWaypoint waypoint : segment.getExtraDataWaypoints().getExtraDataWaypointsInformation()){
					if (waypoint.getLongitude() == 0){
						ecoSpeed = (int)Math.round(waypoint.getLatitude());
						ecoSpeedRouteSegmentInformation.setEcoSpeed(ecoSpeed);
						ecoSpeedRouteSegmentInformation.setEstimatedEmission(calculateEstimatedEmission(ecoSpeed));
						//System.out.println("estimated emission: " + calculateEstimatedEmission(ecoSpeed));
					}else{
						dataWaypoints.getExtraDataWaypointsInformation().add(waypoint);
					}
				}
				System.out.println("Segment in seatsa has " + dataWaypoints.getExtraDataWaypointsInformation().size() + " accidents");
				ecoSpeedRouteSegmentInformation.setWeatherCondition(segment.getWeatherCondition());
				ecoSpeedRouteSegmentInformation.setExtraDataWaypoints(dataWaypoints);
				ecoSpeedRouteSegmentInformation.setRouteSegment(segment.getRouteSegment());
				ecoSpeedRouteInformation.getEcoSpeedRouteSegmentsInformation().add(ecoSpeedRouteSegmentInformation);			
			}
			ecoSpeedRoutesInformationResponse.getEcoSpeedRoutesInformation().add(ecoSpeedRouteInformation);
		}
					
		return ecoSpeedRoutesInformationResponse;
		/**
		*	TODO write the code that generates EcoSpeedRoutesInformationResponse message
		*	that has to be sent to receiverParticipantName within the interaction belonging to choreographyTaskName
		*	You can exploit the following API from choreographyInstanceMessages
		*	1. Get a Message using its name 
		*		- example: 
		*			EcoSpeedRoutesInformationRequest message = (EcoSpeedRoutesInformationRequest) choreographyInstanceMessages.getMessage("EcoSpeedRoutesInformationRequest");
		*		A null value is returned if no message has been found.
		*	2. Get a list of messages using a message name
		*		- example: 
		*			List<EcoSpeedRoutesInformationRequest> messages = choreographyInstanceMessages.getMessages("EcoSpeedRoutesInformationRequest");
		*		A null value is returned if no message has been found. 
		*	3. Get a message using its name and the name of the sender participant
		*		- example: 
		*			EcoSpeedRoutesInformationRequest message = (EcoSpeedRoutesInformationRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoSpeedRoutesInformationRequest","SEADA-SEARP");
		*		A null value is returned if no message has been found. 
		*	4. Get a list of messages using a message name and a sender participant name
		*		- example: 
		*			List<EcoSpeedRoutesInformationRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoSpeedRoutesInformationRequest","SEADA-SEARP");
		* 		A null value is returned if no message has been found.
		*	5. Get a message using its name, the name of the sender participant and the name of the task in which 
		*		the message has been exchanged
		*		- example: 
		*			EcoSpeedRoutesInformationRequest message = (EcoSpeedRoutesInformationRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoSpeedRoutesInformationRequest","SEADA-SEARP","Get Eco Speed Routes Information");	
		*		A null value is returned if no message has been found.
		*	6. Get a list of messages using a message name, the name of the sender participant and the name of the task in which 
		*		the messages has been exchanged
		*		- example: 
		*			List<EcoSpeedRoutesInformationRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoSpeedRoutesInformationRequest","SEADA-SEARP","Get Eco Speed Routes Information");
		*		A null value is returned if no message has been found.
		*	7. Get a message using its name and the name of the receiver participant
		*		- example: 
		*			EcoSpeedRoutesInformationResponse message = (EcoSpeedRoutesInformationResponse) choreographyInstanceMessages.getMessageReceivedParticipant("EcoSpeedRoutesInformationResponse", "SEADA-SEARP");
		*		A null value is returned if no message has been found. 
		*	8. Get a message using its name, the name of the receiver participant and the name of the task in which 
		*		the message has been exchanged
		*		- example: 
		*			EcoSpeedRoutesInformationResponse message = (EcoSpeedRoutesInformationResponse) choreographyInstanceMessages.getMessageReceivedFromParticipant("EcoSpeedRoutesInformationResponse", "SEADA-SEARP", "Set Eco Speed Routes Information");	
		*		A null value is returned if no message has been found.
		*	9. Get a list of messages using a message name and a receiver participant name
		*		- example: 
        *			List<EcoSpeedRoutesInformationResponse> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("EcoSpeedRoutesInformationResponse", "SEADA-SEARP");
		*	10. Get a list of messages using a message name, the name of the receiver participant and the name of the task in which 
		*		the messages has been exchanged
		*		- example: 
		*			List<EcoSpeedRoutesInformationResponse> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("EcoSpeedRoutesInformationResponse", "SEADA-SEARP", "Set Eco Speed Routes Information");
		*		A null value is returned if no message has been found.
		*/		
	}
	
	private double calculateEstimatedEmission(int speed){
		double spdMetersPerSec = speed/3.6;
		return Math.round((0.000065 * Math.pow(spdMetersPerSec, 2) + 0.533 / spdMetersPerSec + 0.09) * 1000.0) / 1000.0;
	}

	@Override
	public TrafficRouteInformationRequest createTrafficRouteInformationRequest(
			ChoreographyInstanceMessages choreographyInstanceMessages, ChoreographyLoopIndexes choreographyLoopIndexes, String choreographyTaskName, String receiverParticipantName) {
		
		System.out.println("reached SEADASEATSAServiceImpl.createTrafficRouteInformationRequest");
		
		TrafficRouteInformationRequest trafficRouteInformationRequest = new TrafficRouteInformationRequest();
		EcoSpeedRoutesInformationRequest ecoSpeedRoutesInformationRequest = (EcoSpeedRoutesInformationRequest) choreographyInstanceMessages.getMessage("EcoSpeedRoutesInformationRequest");
		int indexValue = choreographyLoopIndexes.getChoreographyLoopIndex("EcoFriendlyRouteInformationCollection");
		System.out.println("CREATING TRAFFIC ROUTE INFORMATION REQUEST FOR ROUTE NUMBER: "+indexValue);		
		System.out.println("SEADASEATSAServiceImpl.createTrafficRouteInformationRequest EcoFriendlyRouteInformationCollection index: " + indexValue); //indexValue is 0
		// i take the route from TrafficRouteInformationRequest based on the indexValue
		//i iterate through all waypoints that came from the routes and devide the routes into segments
		Waypoint origin = null;
		for (WaypointInfo waypoint : ecoSpeedRoutesInformationRequest.getRoutes().get(indexValue).getWaypoints()) {
			if (origin != null){
				RouteSegment segment = new RouteSegment();		
				Waypoint start = new Waypoint();
				start.setLat(origin.getLat());
				start.setLon(origin.getLon());
				segment.setStart(start);
				Waypoint destination = new Waypoint();
				destination.setLat(waypoint.getWaypoint().getLat());
				destination.setLon(waypoint.getWaypoint().getLon());
				segment.setEnd(destination);
				origin.setLat(destination.getLat());
				origin.setLon(destination.getLon());
				trafficRouteInformationRequest.getRouteSegments().add(segment);
			} else{
				origin = new Waypoint();
				origin.setLat(waypoint.getWaypoint().getLat());
				origin.setLon(waypoint.getWaypoint().getLon());
			}
		}	
		System.out.println("Number of routeSegments in trafficRouteInformationRequest: " + trafficRouteInformationRequest.getRouteSegments().size());
		System.out.println("ROUTE NUMBER: "+indexValue+" ROUTE SEGMENTS NUMBER: "+trafficRouteInformationRequest.getRouteSegments().size());
		for (RouteSegment segment : trafficRouteInformationRequest.getRouteSegments()) {
			System.out.println("Route segment in trafficRequest: {" + segment.getStart().getLat() + " , " + segment.getStart().getLon() + "}, {" + segment.getEnd().getLat() + " , " + segment.getEnd().getLon() + "}");
		}
		return trafficRouteInformationRequest;
		/**
		*	TODO write the code that generates EcoSpeedRoutesInformationResponse message
		*	that has to be sent to receiverParticipantName within the interaction belonging to choreographyTaskName
		*	You can exploit the following API from choreographyInstanceMessages
		*	1. Get a Message using its name 
		*		- example: 
		*			EcoSpeedRoutesInformationRequest message = (EcoSpeedRoutesInformationRequest) choreographyInstanceMessages.getMessage("EcoSpeedRoutesInformationRequest");
		*		A null value is returned if no message has been found.
		*	2. Get a list of messages using a message name
		*		- example: 
		*			List<EcoSpeedRoutesInformationRequest> messages = choreographyInstanceMessages.getMessages("EcoSpeedRoutesInformationRequest");
		*		A null value is returned if no message has been found. 
		*	3. Get a message using its name and the name of the sender participant
		*		- example: 
		*			EcoSpeedRoutesInformationRequest message = (EcoSpeedRoutesInformationRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoSpeedRoutesInformationRequest","SEADA-SEARP");
		*		A null value is returned if no message has been found. 
		*	4. Get a list of messages using a message name and a sender participant name
		*		- example: 
		*			List<EcoSpeedRoutesInformationRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoSpeedRoutesInformationRequest","SEADA-SEARP");
		* 		A null value is returned if no message has been found.
		*	5. Get a message using its name, the name of the sender participant and the name of the task in which 
		*		the message has been exchanged
		*		- example: 
		*			EcoSpeedRoutesInformationRequest message = (EcoSpeedRoutesInformationRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoSpeedRoutesInformationRequest","SEADA-SEARP","Get Eco Speed Routes Information");	
		*		A null value is returned if no message has been found.
		*	6. Get a list of messages using a message name, the name of the sender participant and the name of the task in which 
		*		the messages has been exchanged
		*		- example: 
		*			List<EcoSpeedRoutesInformationRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoSpeedRoutesInformationRequest","SEADA-SEARP","Get Eco Speed Routes Information");
		*		A null value is returned if no message has been found.
		*	7. Get a message using its name and the name of the receiver participant
		*		- example: 
		*			EcoSpeedRoutesInformationResponse message = (EcoSpeedRoutesInformationResponse) choreographyInstanceMessages.getMessageReceivedParticipant("EcoSpeedRoutesInformationResponse", "SEADA-SEARP");
		*		A null value is returned if no message has been found. 
		*	8. Get a message using its name, the name of the receiver participant and the name of the task in which 
		*		the message has been exchanged
		*		- example: 
		*			EcoSpeedRoutesInformationResponse message = (EcoSpeedRoutesInformationResponse) choreographyInstanceMessages.getMessageReceivedFromParticipant("EcoSpeedRoutesInformationResponse", "SEADA-SEARP", "Set Eco Speed Routes Information");	
		*		A null value is returned if no message has been found.
		*	9. Get a list of messages using a message name and a receiver participant name
		*		- example: 
        *			List<EcoSpeedRoutesInformationResponse> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("EcoSpeedRoutesInformationResponse", "SEADA-SEARP");
		*	10. Get a list of messages using a message name, the name of the receiver participant and the name of the task in which 
		*		the messages has been exchanged
		*		- example: 
		*			List<EcoSpeedRoutesInformationResponse> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("EcoSpeedRoutesInformationResponse", "SEADA-SEARP", "Set Eco Speed Routes Information");
		*		A null value is returned if no message has been found.
		*/					
		
	
		/**
		 * 	NOTE: The creation of this message is part of a choreography task involved in a loop. 
		 * 	Using the name of the choreography task or sub-choreography looped you can get the value of 
		 * 	the related index and exploit this to retrieve information in order to generate EcoRoutesResponse message
		 * 		- example: 
		 * 			int indexValue = choreographyLoopIndexes.getChoreographyLoopIndex("EcoFriendlyRouteInformationCollection");
		 * 		The value -1 is returned if no index value has been found.
		 * 	NOTE: Each index item numbering begins with 0.
		 * 
		 * 	NOTE: The value of EcoFriendlyRouteInformationCollection index is available only when Eco Friendly Route Information Collection
		 *  	is triggered by SEADA-SEATSA after Get Eco Speed Route Information choreography task
		 */		
	}

	@Override
	public void getEcoSpeedRoutesInformation(EcoSpeedRoutesInformationRequest ecoSpeedRoutesInformationRequest, String senderParticipantName) {
		
		/**
		*	TODO Add your business logic upon the reception of EcoSpeedRoutesInformationRequest message from senderParticipantName 
		*/	
	}

	@Override
	public void setRouteTrafficInformation(TrafficRouteInformationResponse trafficRouteInformationResponse, String senderParticipantName) {
		
		/**
		*	TODO Add your business logic upon the reception of TrafficRouteInformationResponse message from senderParticipantName
		*/	
	}

	@Override
	public void getEcoSpeedRouteInformation(EcoSpeedRouteInformationRequest ecoSpeedRouteInformationRequest, String senderParticipantName) {
		
		/**
		*	TODO Add your business logic upon the reception of EcoSpeedRouteInformationRequest message from senderParticipantName 
		*/
	}

	@Override
	public EcoSpeedRouteInformationResponse createEcoSpeedRouteInformationResponse(
			ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
		
		System.out.println("reached SEADASEATSAServiceImpl.createEcoSpeedRouteInformationResponse");
		
		EcoSpeedRouteInformationResponse ecoSpeedRouteInformationResponse = new EcoSpeedRouteInformationResponse();
		
		TrafficRouteInformationResponse trafficRouteInformationResponse = (TrafficRouteInformationResponse) choreographyInstanceMessages.getMessage("TrafficRouteInformationResponse");
		EcoSpeedRouteInformation ecoSpeedRouteInformation = new EcoSpeedRouteInformation();
		for(TrafficRouteSegmentInformation segment : trafficRouteInformationResponse.getTrafficRouteInformation()){
			EcoSpeedRouteSegmentInformation ecoSpeedRouteSegmentInformation = new EcoSpeedRouteSegmentInformation();
			ecoSpeedRouteSegmentInformation.setCongestionLevel(segment.getCongestionLevel());
			ecoSpeedRouteSegmentInformation.setEcoLevel(1);
			ecoSpeedRouteSegmentInformation.setEcoSpeed(50);
			ecoSpeedRouteSegmentInformation.setEstimatedEmission(0.25);
			ecoSpeedRouteSegmentInformation.setWeatherCondition(segment.getWeatherCondition());
			ecoSpeedRouteSegmentInformation.setExtraDataWaypoints(segment.getExtraDataWaypoints());
			ecoSpeedRouteSegmentInformation.setRouteSegment(segment.getRouteSegment());
			ecoSpeedRouteInformation.getEcoSpeedRouteSegmentsInformation().add(ecoSpeedRouteSegmentInformation);			
		}
		ecoSpeedRouteInformationResponse.setEcoSpeedRouteInformation(ecoSpeedRouteInformation);
							
		return ecoSpeedRouteInformationResponse;
		/**
		*	TODO write the code that generates EcoSpeedRoutesInformationResponse message
		*	that has to be sent to receiverParticipantName within the interaction belonging to choreographyTaskName
		*	You can exploit the following API from choreographyInstanceMessages
		*	1. Get a Message using its name 
		*		- example: 
		*			EcoSpeedRoutesInformationRequest message = (EcoSpeedRoutesInformationRequest) choreographyInstanceMessages.getMessage("EcoSpeedRoutesInformationRequest");
		*		A null value is returned if no message has been found.
		*	2. Get a list of messages using a message name
		*		- example: 
		*			List<EcoSpeedRoutesInformationRequest> messages = choreographyInstanceMessages.getMessages("EcoSpeedRoutesInformationRequest");
		*		A null value is returned if no message has been found. 
		*	3. Get a message using its name and the name of the sender participant
		*		- example: 
		*			EcoSpeedRoutesInformationRequest message = (EcoSpeedRoutesInformationRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoSpeedRoutesInformationRequest","SEADA-SEARP");
		*		A null value is returned if no message has been found. 
		*	4. Get a list of messages using a message name and a sender participant name
		*		- example: 
		*			List<EcoSpeedRoutesInformationRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoSpeedRoutesInformationRequest","SEADA-SEARP");
		* 		A null value is returned if no message has been found.
		*	5. Get a message using its name, the name of the sender participant and the name of the task in which 
		*		the message has been exchanged
		*		- example: 
		*			EcoSpeedRoutesInformationRequest message = (EcoSpeedRoutesInformationRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoSpeedRoutesInformationRequest","SEADA-SEARP","Get Eco Speed Routes Information");	
		*		A null value is returned if no message has been found.
		*	6. Get a list of messages using a message name, the name of the sender participant and the name of the task in which 
		*		the messages has been exchanged
		*		- example: 
		*			List<EcoSpeedRoutesInformationRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoSpeedRoutesInformationRequest","SEADA-SEARP","Get Eco Speed Routes Information");
		*		A null value is returned if no message has been found.
		*	7. Get a message using its name and the name of the receiver participant
		*		- example: 
		*			EcoSpeedRoutesInformationResponse message = (EcoSpeedRoutesInformationResponse) choreographyInstanceMessages.getMessageReceivedParticipant("EcoSpeedRoutesInformationResponse", "SEADA-SEARP");
		*		A null value is returned if no message has been found. 
		*	8. Get a message using its name, the name of the receiver participant and the name of the task in which 
		*		the message has been exchanged
		*		- example: 
		*			EcoSpeedRoutesInformationResponse message = (EcoSpeedRoutesInformationResponse) choreographyInstanceMessages.getMessageReceivedFromParticipant("EcoSpeedRoutesInformationResponse", "SEADA-SEARP", "Set Eco Speed Routes Information");	
		*		A null value is returned if no message has been found.
		*	9. Get a list of messages using a message name and a receiver participant name
		*		- example: 
        *			List<EcoSpeedRoutesInformationResponse> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("EcoSpeedRoutesInformationResponse", "SEADA-SEARP");
		*	10. Get a list of messages using a message name, the name of the receiver participant and the name of the task in which 
		*		the messages has been exchanged
		*		- example: 
		*			List<EcoSpeedRoutesInformationResponse> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("EcoSpeedRoutesInformationResponse", "SEADA-SEARP", "Set Eco Speed Routes Information");
		*		A null value is returned if no message has been found.
		*/		
	}

}
