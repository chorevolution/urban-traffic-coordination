package eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.ChoreographyLoopIndex;

public class ChoreographyLoopIndexes {
	
	private Map<String, Integer> loopIndexes;
	
	public ChoreographyLoopIndexes(List<ChoreographyLoopIndex> choreographyLoopIndexes){
		
		loopIndexes = Collections.synchronizedMap(new HashMap<>());
		for (ChoreographyLoopIndex choreographyLoopIndex : choreographyLoopIndexes) {
			loopIndexes.put(choreographyLoopIndex.getIndexName(), choreographyLoopIndex.getIndexValue()-1);
		}
	}

	public int getChoreographyLoopIndex(String choreographyIndexName){
		
		if(loopIndexes.containsKey(choreographyIndexName))
			return loopIndexes.get(choreographyIndexName).intValue();
		else
			return -1;
	}
}

