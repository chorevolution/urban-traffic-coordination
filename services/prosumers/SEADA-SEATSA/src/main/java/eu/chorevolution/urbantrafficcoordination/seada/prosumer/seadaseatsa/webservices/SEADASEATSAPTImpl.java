package eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRouteInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRoutesInformationRequestReturnType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRoutesInformationRequestType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.EcoSpeedRoutesInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.GetEcoSpeedRouteInformationReturnType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.GetEcoSpeedRouteInformationType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.SEADASEATSAPT;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.SendLoopRequestType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.SendRequestType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.TrafficRouteInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.TrafficRouteInformationResponseReturnType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.TrafficRouteInformationResponseType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.business.ChoreographyDataService;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.business.ChoreographyInstanceMessagesStore;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.business.SEADASEATSAService;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadaseatsa.model.ChoreographyLoopIndexes;

@Component(value="SEADASEATSAPTImpl")
public class SEADASEATSAPTImpl implements SEADASEATSAPT{
	
	private static Logger logger = LoggerFactory.getLogger(SEADASEATSAPTImpl.class);
	
	@Autowired
	private SEADASEATSAService service;
	@Autowired
	private ChoreographyDataService choreographyDataService;
	
	@Override
	public EcoSpeedRoutesInformationResponse sendEcoSpeedRoutesInformationResponse(SendRequestType parameters) {
		
		logger.info("CALLED sendEcoSpeedRoutesInformationResponse ON SEADA-SEATSA");
		logger.info("START TIME OPERATION sendEcoSpeedRoutesInformationResponse ON SEADA-SEATSA: "+System.currentTimeMillis());		
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		EcoSpeedRoutesInformationResponse ecoSpeedRoutesInformationResponse = service.createEcoSpeedRoutesInformationResponse(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), ecoSpeedRoutesInformationResponse);
		logger.info("END TIME OPERATION sendEcoSpeedRoutesInformationResponse ON SEADA-SEATSA: "+System.currentTimeMillis());		
		return ecoSpeedRoutesInformationResponse;
	}	
	@Override
	public TrafficRouteInformationRequest sendTrafficRouteInformationRequest(SendLoopRequestType parameters) {

		logger.info("CALLED sendTrafficRouteInformationRequest ON SEADA-SEATSA");
		logger.info("START TIME OPERATION sendTrafficRouteInformationRequest ON SEADA-SEATSA: "+System.currentTimeMillis());		
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());
		TrafficRouteInformationRequest trafficRouteInformationRequest = service.createTrafficRouteInformationRequest(store, choreographyLoopIndexes, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), trafficRouteInformationRequest);		
		logger.info("END TIME OPERATION sendTrafficRouteInformationRequest ON SEADA-SEATSA: "+System.currentTimeMillis());	
		return trafficRouteInformationRequest;
	}
	@Override
	public EcoSpeedRoutesInformationRequestReturnType getEcoSpeedRoutesInformation(EcoSpeedRoutesInformationRequestType parameters) {

		logger.info("CALLED getEcoSpeedRoutesInformation ON SEADA-SEATSA");
		logger.info("START TIME OPERATION getEcoSpeedRoutesInformation ON SEADA-SEATSA: "+System.currentTimeMillis());
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), parameters.getMessageData());
		service.getEcoSpeedRoutesInformation(parameters.getMessageData(), parameters.getSenderParticipantName());
		logger.info("END TIME OPERATION getEcoSpeedRoutesInformation ON SEADA-SEATSA: "+System.currentTimeMillis());		
		return new EcoSpeedRoutesInformationRequestReturnType();
	}
	@Override
	public TrafficRouteInformationResponseReturnType setRouteTrafficInformation(TrafficRouteInformationResponseType parameters) {

		logger.info("CALLED setRouteTrafficInformation ON SEADA-SEATSA");
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), parameters.getMessageData());			
		service.setRouteTrafficInformation(parameters.getMessageData(), parameters.getSenderParticipantName());
		return new TrafficRouteInformationResponseReturnType();
	}
	
	@Override
	public GetEcoSpeedRouteInformationReturnType getEcoSpeedRouteInformation(GetEcoSpeedRouteInformationType parameters) {

		logger.info("CALLED getEcoSpeedRouteInformation ON SEADA-SEATSA");
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), parameters.getMessageData());	
		service.getEcoSpeedRouteInformation(parameters.getMessageData(), parameters.getSenderParticipantName());
		return new GetEcoSpeedRouteInformationReturnType();
	}
	@Override
	public EcoSpeedRouteInformationResponse sendEcoSpeedRouteInformationResponse(SendRequestType parameters) {

		logger.info("CALLED sendEcoSpeedRouteInformationResponse ON SEADA-SEATSA");
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		EcoSpeedRouteInformationResponse ecoSpeedRouteInformationResponse = service.createEcoSpeedRouteInformationResponse(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), ecoSpeedRouteInformationResponse);
		return ecoSpeedRouteInformationResponse;
	}	

}
