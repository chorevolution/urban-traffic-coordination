package eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.business.impl.dataservice;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.business.ChoreographyDataService;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.business.ChoreographyInstanceMessagesStore;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.model.MessagesData;

@Service
public class ChoreographiesDataServiceImpl<T> implements ChoreographyDataService {
	// choregraphyID, ChoreographyInstanceMessages
	private Map<String, ChoreographyInstanceMessagesStore<T>> choreographyInstancesMessages = Collections.synchronizedMap(new HashMap<String, ChoreographyInstanceMessagesStore<T>>());

	public ChoreographyInstanceMessagesStore<T> getChoreographyInstanceMessages(String choreographyID) {
		
		if(!choreographyInstancesMessages.containsKey(choreographyID)){
			Map<String, List<MessagesData<T>>> map = Collections.synchronizedMap(new HashMap<String, List<MessagesData<T>>>());
			ChoreographyInstanceMessagesStore<T> choreographyInstanceMessages = new ChoreographyInstanceMessagesStoreImpl<T>(map);
			choreographyInstancesMessages.put(choreographyID, choreographyInstanceMessages);
		}			
		return choreographyInstancesMessages.get(choreographyID);
	}
	
}
