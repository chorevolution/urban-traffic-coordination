package eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.business;

public interface ChoreographyDataService {

	ChoreographyInstanceMessagesStore getChoreographyInstanceMessages(String choreographyID);
	
}
