package eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.AccidentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.BridgeStatusInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.CongestionStatusRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.ReceiveBridgeStatusInformationResponseReturnType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.ReceiveBridgeStatusInformationResponseType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.ReceiveCongestionInformationResponseReturnType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.ReceiveCongestionInformationResponseType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.ReceiveDestinationAccidentInformationResponseReturnType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.ReceiveDestinationAccidentInformationResponseType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.ReceiveDestinationWeatherInformationResponseReturnType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.ReceiveDestinationWeatherInformationResponseType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.ReceiveOriginAccidentInformationResponseReturnType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.ReceiveOriginAccidentInformationResponseType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.ReceiveOriginWeatherInformationResponseReturnType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.ReceiveOriginWeatherInformationResponseType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.SEADATRAFFICPT;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.SendLoopRequestType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.TrafficRouteInformationRequestReturnType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.TrafficRouteInformationRequestType;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.TrafficRouteInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.WeatherInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.business.ChoreographyDataService;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.business.ChoreographyInstanceMessagesStore;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.business.SEADATRAFFICService;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.model.ChoreographyLoopIndexes;

@Component(value="SEADATRAFFICPTImpl")
public class SEADATRAFFICPTImpl implements SEADATRAFFICPT{

	private static Logger logger = LoggerFactory.getLogger(SEADATRAFFICPTImpl.class);

	@Autowired
	private SEADATRAFFICService service;
	@Autowired
	private ChoreographyDataService choreographyDataService;
	
	@Override
	public ReceiveBridgeStatusInformationResponseReturnType receiveBridgeStatusInformationResponse(
			ReceiveBridgeStatusInformationResponseType parameters) {
		
		logger.info("CALLED receiveBridgeStatusInformationResponse ON SEADA-TRAFFIC");
		logger.info("START TIME OPERATION receiveBridgeStatusInformationResponse ON SEADA-TRAFFIC: "+System.currentTimeMillis());			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), parameters.getMessageData());
		service.receiveBridgeStatusInformationResponse(parameters.getMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		logger.info("END TIME OPERATION receiveBridgeStatusInformationResponse ON SEADA-TRAFFIC: "+System.currentTimeMillis());	
		return new ReceiveBridgeStatusInformationResponseReturnType();
	}
	@Override
	public AccidentInformationRequest sendDestinationAccidentInformationRequest(
			SendLoopRequestType parameters) {
		
		logger.info("CALLED sendDestinationAccidentInformationRequest ON SEADA-TRAFFIC");
		logger.info("START TIME OPERATION sendDestinationAccidentInformationRequest ON SEADA-TRAFFIC: "+System.currentTimeMillis());			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());
		AccidentInformationRequest  accidentInformationRequest = service.createDestinationAccidentInformationRequest(store, choreographyLoopIndexes, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), accidentInformationRequest);		
		logger.info("END TIME OPERATION sendDestinationAccidentInformationRequest ON SEADA-TRAFFIC: "+System.currentTimeMillis());	
		return accidentInformationRequest;
	}
	@Override
	public WeatherInformationRequest sendOriginWeatherInformationRequest(SendLoopRequestType parameters) {
		
		logger.info("CALLED sendOriginWeatherInformationRequest ON SEADA-TRAFFIC");
		logger.info("START TIME OPERATION sendOriginWeatherInformationRequest ON SEADA-TRAFFIC: "+System.currentTimeMillis());			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());		
		WeatherInformationRequest weatherInformationRequest = service.createOriginWeatherInformationRequest(store, choreographyLoopIndexes, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), weatherInformationRequest);		
		logger.info("END TIME OPERATION sendOriginWeatherInformationRequest ON SEADA-TRAFFIC: "+System.currentTimeMillis());	
		return weatherInformationRequest;
	}
	@Override
	public ReceiveDestinationWeatherInformationResponseReturnType receiveDestinationWeatherInformationResponse(
			ReceiveDestinationWeatherInformationResponseType parameters) {
		
		logger.info("CALLED receiveDestinationWeatherInformationResponse ON SEADA-TRAFFIC");
		logger.info("START TIME OPERATION receiveDestinationWeatherInformationResponse ON SEADA-TRAFFIC: "+System.currentTimeMillis());			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), parameters.getMessageData());
		service.receiveDestinationWeatherInformationResponse(parameters.getMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		logger.info("END TIME OPERATION receiveDestinationWeatherInformationResponse ON SEADA-TRAFFIC: "+System.currentTimeMillis());			
		return new ReceiveDestinationWeatherInformationResponseReturnType();
	}
	@Override
	public AccidentInformationRequest sendOriginAccidentInformationRequest(SendLoopRequestType parameters) {
		
		logger.info("CALLED sendOriginAccidentInformationRequest ON SEADA-TRAFFIC");
		logger.info("START TIME OPERATION sendOriginAccidentInformationRequest ON SEADA-TRAFFIC: "+System.currentTimeMillis());		
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());	
		AccidentInformationRequest accidentInformationRequest = service.createOriginAccidentInformationRequest(store, choreographyLoopIndexes, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), accidentInformationRequest);			
		logger.info("END TIME OPERATION sendOriginAccidentInformationRequest ON SEADA-TRAFFIC: "+System.currentTimeMillis());	
		return accidentInformationRequest;
	}
	@Override
	public TrafficRouteInformationResponse sendTrafficRouteInformationResponse(SendLoopRequestType parameters) {
		
		logger.info("CALLED sendTrafficRouteInformationResponse ON SEADA-TRAFFIC");
		logger.info("START TIME OPERATION sendTrafficRouteInformationResponse ON SEADA-TRAFFIC: "+System.currentTimeMillis());			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());	
		TrafficRouteInformationResponse trafficRouteInformationResponse = service.createTrafficRouteInformationResponse(store, choreographyLoopIndexes, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), trafficRouteInformationResponse);					
		logger.info("START TIME OPERATION sendTrafficRouteInformationResponse ON SEADA-TRAFFIC: "+System.currentTimeMillis());	
		return trafficRouteInformationResponse;
	}
	@Override
	public ReceiveCongestionInformationResponseReturnType receiveCongestionStatusResponse(
			ReceiveCongestionInformationResponseType parameters) {
		
		logger.info("CALLED receiveCongestionStatusResponse ON SEADA-TRAFFIC");	
		logger.info("START TIME OPERATION receiveCongestionStatusResponse ON SEADA-TRAFFIC: "+System.currentTimeMillis());			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), parameters.getMessageData());
		service.receiveCongestionStatusResponse(parameters.getMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		logger.info("END TIME OPERATION receiveCongestionStatusResponse ON SEADA-TRAFFIC: "+System.currentTimeMillis());	
		return new ReceiveCongestionInformationResponseReturnType();
	}
	@Override
	public ReceiveOriginAccidentInformationResponseReturnType receiveOriginAccidentInformationResponse(
			ReceiveOriginAccidentInformationResponseType parameters) {
		
		logger.info("CALLED receiveOriginAccidentInformationResponse ON SEADA-TRAFFIC");
		logger.info("START TIME OPERATION receiveOriginAccidentInformationResponse ON SEADA-TRAFFIC: "+System.currentTimeMillis());			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), parameters.getMessageData());
		service.receiveOriginAccidentInformationResponse(parameters.getMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		logger.info("END TIME OPERATION receiveOriginAccidentInformationResponse ON SEADA-TRAFFIC: "+System.currentTimeMillis());				
		return new ReceiveOriginAccidentInformationResponseReturnType();
	}
	@Override
	public ReceiveDestinationAccidentInformationResponseReturnType receiveDestinationAccidentInformationResponse(
			ReceiveDestinationAccidentInformationResponseType parameters) {
		
		logger.info("CALLED receiveDestinationAccidentInformationResponse ON SEADA-TRAFFIC");
		logger.info("START TIME OPERATION receiveDestinationAccidentInformationResponse ON SEADA-TRAFFIC: "+System.currentTimeMillis());			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), parameters.getMessageData());
		service.receiveDestinationAccidentInformationResponse(parameters.getMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		logger.info("END TIME OPERATION receiveDestinationAccidentInformationResponse ON SEADA-TRAFFIC: "+System.currentTimeMillis());				
		return new ReceiveDestinationAccidentInformationResponseReturnType();
	}
	@Override
	public BridgeStatusInformationRequest sendBridgeStatusInformationRequest(SendLoopRequestType parameters) {
		
		logger.info("CALLED sendBridgeStatusInformationRequest ON SEADA-TRAFFIC");
		logger.info("START TIME OPERATION sendBridgeStatusInformationRequest ON SEADA-TRAFFIC: "+System.currentTimeMillis());		
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());		
		BridgeStatusInformationRequest bridgeStatusInformationRequest = service.createBridgeStatusInformationRequest(store, choreographyLoopIndexes, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), bridgeStatusInformationRequest);			
		logger.info("END TIME OPERATION sendBridgeStatusInformationRequest ON SEADA-TRAFFIC: "+System.currentTimeMillis());	
		return bridgeStatusInformationRequest;
	}
	@Override
	public CongestionStatusRequest sendCongestionStatusRequest(SendLoopRequestType parameters) {
		
		logger.info("CALLED sendCongestionStatusRequest ON SEADA-TRAFFIC");
		logger.info("START TIME OPERATION sendCongestionStatusRequest ON SEADA-TRAFFIC: "+System.currentTimeMillis());			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());			
		CongestionStatusRequest congestionStatusRequest = service.createCongestionStatusRequest(store, choreographyLoopIndexes, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), congestionStatusRequest);			
		logger.info("END TIME OPERATION sendCongestionStatusRequest ON SEADA-TRAFFIC: "+System.currentTimeMillis());	
		return congestionStatusRequest;
	}
	@Override
	public ReceiveOriginWeatherInformationResponseReturnType receiveOriginWeatherInformationResponse(
			ReceiveOriginWeatherInformationResponseType parameters) {
		
		logger.info("CALLED receiveOriginWeatherInformationResponse ON SEADA-TRAFFIC");
		logger.info("START TIME OPERATION receiveOriginWeatherInformationResponse ON SEADA-TRAFFIC: "+System.currentTimeMillis());			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(),parameters.getMessageData());
		service.receiveOriginWeatherInformationResponse(parameters.getMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		logger.info("END TIME OPERATION receiveOriginWeatherInformationResponse ON SEADA-TRAFFIC: "+System.currentTimeMillis());		
		return new ReceiveOriginWeatherInformationResponseReturnType();
	}
	@Override
	public TrafficRouteInformationRequestReturnType getRouteTrafficInformation(TrafficRouteInformationRequestType parameters) {
		
		logger.info("CALLED getRouteTrafficInformation ON SEADA-TRAFFIC");		
		logger.info("START TIME OPERATION getRouteTrafficInformation ON SEADA-TRAFFIC: "+System.currentTimeMillis());			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), parameters.getMessageData());
		service.getRouteTrafficInformation(parameters.getMessageData(), parameters.getSenderParticipantName());
		logger.info("END TIME OPERATION getRouteTrafficInformation ON SEADA-TRAFFIC: "+System.currentTimeMillis());			
		return new TrafficRouteInformationRequestReturnType();
	}
	@Override
	public WeatherInformationRequest sendDestinationWeatherInformationRequest(
			SendLoopRequestType parameters) {
		
		logger.info("CALLED sendDestinationWeatherInformationRequest ON SEADA-TRAFFIC");
		logger.info("START TIME OPERATION sendDestinationWeatherInformationRequest ON SEADA-TRAFFIC: "+System.currentTimeMillis());			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());			
		WeatherInformationRequest weatherInformationRequest = service.createDestinationWeatherInformationRequest(store, choreographyLoopIndexes, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getMessageName(), parameters.getChoreographyTaskName(), weatherInformationRequest);			
		logger.info("END TIME OPERATION sendDestinationWeatherInformationRequest ON SEADA-TRAFFIC: "+System.currentTimeMillis());	
		return weatherInformationRequest;
	}
	
}
