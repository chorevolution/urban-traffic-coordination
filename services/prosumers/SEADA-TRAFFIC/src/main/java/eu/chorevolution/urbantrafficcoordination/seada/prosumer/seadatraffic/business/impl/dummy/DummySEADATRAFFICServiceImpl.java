package eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.AccidentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.AccidentInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.BridgeStatusInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.BridgeStatusInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.CongestionStatusRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.CongestionStatusResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.RouteSegment;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.RouteSegmentData;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.TrafficRouteInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.TrafficRouteInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.TrafficRouteSegmentInformation;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.Waypoint;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.WeatherInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.WeatherInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.business.ChoreographyInstanceMessages;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.business.SEADATRAFFICService;
import eu.chorevolution.urbantrafficcoordination.seada.prosumer.seadatraffic.model.ChoreographyLoopIndexes;

@Service
public class DummySEADATRAFFICServiceImpl implements SEADATRAFFICService{

	private static Logger logger = LoggerFactory.getLogger(DummySEADATRAFFICServiceImpl.class);

	@Override
	public void receiveBridgeStatusInformationResponse(
			BridgeStatusInformationResponse bridgeStatusInformationResponse, String choreographyTaskName, String senderParticipantName) {
		
		logger.info("CALLED receiveBridgeStatusInformationResponse BUSINESS-LOGIC ON SEADA-TRAFFIC");
		
	}

	@Override
	public AccidentInformationRequest createDestinationAccidentInformationRequest(
			ChoreographyInstanceMessages choreographyInstanceMessages, ChoreographyLoopIndexes choreographyLoopIndexes, String choreographyTaskName, String receiverParticipantName) {
		
		logger.info("CALLED createDestinationAccidentInformationRequest BUSINESS-LOGIC ON SEADA-TRAFFIC");
		// choreographyLoopIndexes.getChoreographyLoopIndex("RouteSegmentTrafficInformationCollection");
		AccidentInformationRequest accidentInformationRequest = new AccidentInformationRequest();
		accidentInformationRequest.setLat(11);
		accidentInformationRequest.setLon(22);
		return accidentInformationRequest;
	}

	@Override
	public WeatherInformationRequest createOriginWeatherInformationRequest(
			ChoreographyInstanceMessages choreographyInstanceMessages, ChoreographyLoopIndexes choreographyLoopIndexes, String choreographyTaskName, String receiverParticipantName) {
		
		logger.info("CALLED createOriginWeatherInformationRequest BUSINESS-LOGIC ON SEADA-TRAFFIC");
		// choreographyLoopIndexes.getChoreographyLoopIndex("RouteSegmentTrafficInformationCollection");
		WeatherInformationRequest weatherInformationRequest = new WeatherInformationRequest();
		weatherInformationRequest.setLat(11);
		weatherInformationRequest.setLon(22);
		return weatherInformationRequest;
	}

	@Override
	public void receiveDestinationWeatherInformationResponse(
			WeatherInformationResponse destinationWeatherInformationResponse, String choreographyTaskName, String senderParticipantName) {
		
		logger.info("CALLED receiveDestinationWeatherInformationResponse BUSINESS-LOGIC ON SEADA-TRAFFIC");
		
	}

	@Override
	public AccidentInformationRequest createOriginAccidentInformationRequest(
			ChoreographyInstanceMessages choreographyInstanceMessages, ChoreographyLoopIndexes choreographyLoopIndexes, String choreographyTaskName, String receiverParticipantName) {
		
		logger.info("CALLED createOriginAccidentInformationRequest BUSINESS-LOGIC ON SEADA-TRAFFIC");
		// choreographyLoopIndexes.getChoreographyLoopIndex("RouteSegmentTrafficInformationCollection");
		AccidentInformationRequest accidentInformationRequest = new AccidentInformationRequest();
		accidentInformationRequest.setLat(11);
		accidentInformationRequest.setLon(22);
		return accidentInformationRequest;
	}

	@Override
	public TrafficRouteInformationResponse createTrafficRouteInformationResponse(
			ChoreographyInstanceMessages choreographyInstanceMessages, ChoreographyLoopIndexes choreographyLoopIndexes, String choreographyTaskName, String receiverParticipantName) {
		
		logger.info("CALLED createTrafficRouteInformationResponse BUSINESS-LOGIC ON SEADA-TRAFFIC");
		// 	choreographyLoopIndexes.getChoreographyLoopIndex("EcoFriendlyRouteInformationCollection");
		TrafficRouteInformationResponse trafficRouteInformationResponse = new TrafficRouteInformationResponse();
		TrafficRouteSegmentInformation trafficRouteSegmentInformation = new TrafficRouteSegmentInformation();
		trafficRouteSegmentInformation.setCongestionLevel(1);
		trafficRouteInformationResponse.getTrafficRouteInformation().add(trafficRouteSegmentInformation);
		return trafficRouteInformationResponse;
	}

	@Override
	public void receiveCongestionStatusResponse(CongestionStatusResponse congestionStatusResponse, String choreographyTaskName, String senderParticipantName) {
		
		logger.info("CALLED receiveCongestionStatusResponse BUSINESS-LOGIC ON SEADA-TRAFFIC");
	}

	@Override
	public void receiveOriginAccidentInformationResponse(
			AccidentInformationResponse accidentInformationResponse, String choreographyTaskName, String senderParticipantName) {
		
		logger.info("CALLED receiveOriginAccidentInformationResponse BUSINESS-LOGIC ON SEADA-TRAFFIC");
	}

	@Override
	public void receiveDestinationAccidentInformationResponse(
			AccidentInformationResponse accidentInformationResponse, String choreographyTaskName, String senderParticipantName) {
		
		logger.info("CALLED receiveDestinationAccidentInformationResponse BUSINESS-LOGIC ON SEADA-TRAFFIC");
	}

	@Override
	public BridgeStatusInformationRequest createBridgeStatusInformationRequest(
			ChoreographyInstanceMessages choreographyInstanceMessages, ChoreographyLoopIndexes choreographyLoopIndexes, String choreographyTaskName, String receiverParticipantName) {
		
		logger.info("CALLED createBridgeStatusInformationRequest BUSINESS-LOGIC ON SEADA-TRAFFIC");
		// choreographyLoopIndexes.getChoreographyLoopIndex("RouteSegmentTrafficInformationCollection");		
		BridgeStatusInformationRequest bridgeStatusInformationRequest = new BridgeStatusInformationRequest();
		Waypoint waypoint = new Waypoint();
		waypoint.setLat(11);
		waypoint.setLon(22);
		bridgeStatusInformationRequest.setDestination(waypoint);
		return bridgeStatusInformationRequest;
	}

	@Override
	public CongestionStatusRequest createCongestionStatusRequest(
			ChoreographyInstanceMessages choreographyInstanceMessages, ChoreographyLoopIndexes choreographyLoopIndexes, String choreographyTaskName, String receiverParticipantName) {
		
		logger.info("CALLED createCongestionStatusRequest BUSINESS-LOGIC ON SEADA-TRAFFIC");
		// 	choreographyLoopIndexes.getChoreographyLoopIndex("EcoFriendlyRouteInformationCollection");
		CongestionStatusRequest congestionStatusRequest = new CongestionStatusRequest();
		RouteSegmentData routeSegmentData = new RouteSegmentData();
		RouteSegment routeSegment = new RouteSegment();
		Waypoint waypoint = new Waypoint();
		waypoint.setLat(11);
		waypoint.setLon(22);
		routeSegment.setStart(waypoint);
		routeSegmentData.setRouteSegment(routeSegment);
		congestionStatusRequest.getRouteSegments().add(routeSegmentData);
		return congestionStatusRequest;
	}

	@Override
	public void receiveOriginWeatherInformationResponse(
			WeatherInformationResponse weatherInformationResponse, String choreographyTaskName, String senderParticipantName) {
		
		logger.info("CALLED receiveOriginWeatherInformationResponse BUSINESS-LOGIC ON SEADA-TRAFFIC");
	}

	@Override
	public void getRouteTrafficInformation(TrafficRouteInformationRequest trafficRouteInformationRequest, String senderParticipantName) {
		
		logger.info("CALLED getRouteTrafficInformation BUSINESS-LOGIC ON SEADA-TRAFFIC");
	}

	@Override
	public WeatherInformationRequest createDestinationWeatherInformationRequest(
			ChoreographyInstanceMessages choreographyInstanceMessages, ChoreographyLoopIndexes choreographyLoopIndexes, String choreographyTaskName, String receiverParticipantName) {
		
		logger.info("CALLED createDestinationWeatherInformationRequest BUSINESS-LOGIC ON SEADA-TRAFFIC");
		// choreographyLoopIndexes.getChoreographyLoopIndex("RouteSegmentTrafficInformationCollection");		
		WeatherInformationRequest weatherInformationRequest = new WeatherInformationRequest();
		weatherInformationRequest.setLat(11);
		weatherInformationRequest.setLon(22);
		return weatherInformationRequest;
	}	
}
