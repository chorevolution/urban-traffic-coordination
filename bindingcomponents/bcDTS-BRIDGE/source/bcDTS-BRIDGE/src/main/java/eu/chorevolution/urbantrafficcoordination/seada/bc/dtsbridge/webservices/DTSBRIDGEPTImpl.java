package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.BridgeStatusInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.BridgeStatusInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.DTSBRIDGEPT;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.business.DTSBridgeService;

@Component(value = "DTSBRIDGEPTImpl")
public class DTSBRIDGEPTImpl implements DTSBRIDGEPT{
	
	private static Logger logger = LoggerFactory.getLogger(DTSBRIDGEPTImpl.class);
	
	@Autowired
	DTSBridgeService service;
	
	@Override
	public BridgeStatusInformationResponse routeSegmentBridgeStatusInformation(
			BridgeStatusInformationRequest parameters) {
		
		logger.info("CALLED routeSegmentBridgeStatusInformation ON bcDTS-BRIDGE");
		logger.info("START TIME OPERATION routeSegmentBridgeStatusInformation ON bcDTS-BRIDGE: "+System.currentTimeMillis());	
		BridgeStatusInformationResponse bridgeStatusInformationResponse = service.getRouteSegmentBridgeStatusInformation(parameters);
		logger.info("END TIME OPERATION routeSegmentBridgeStatusInformation ON bcDTS-BRIDGE: "+System.currentTimeMillis());		
		return bridgeStatusInformationResponse;
	}
}
