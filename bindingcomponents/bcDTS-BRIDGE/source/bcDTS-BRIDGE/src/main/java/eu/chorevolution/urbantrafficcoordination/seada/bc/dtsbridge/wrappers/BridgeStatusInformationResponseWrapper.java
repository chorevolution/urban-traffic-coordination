package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.BridgeStatusInformationResponse;

public class BridgeStatusInformationResponseWrapper {

	private BridgeStatusInformationResponse bridgeStatusInformationResponse;

	public BridgeStatusInformationResponse getBridgeStatusInformationResponse() {
		return bridgeStatusInformationResponse;
	}

	public void setBridgeStatusInformationResponse(BridgeStatusInformationResponse bridgeStatusInformationResponse) {
		this.bridgeStatusInformationResponse = bridgeStatusInformationResponse;
	}
	
	
	
}
