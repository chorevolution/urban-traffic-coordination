package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.business;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.AccidentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.AccidentInformationResponse;

public interface DTSAccidentsService {
	AccidentInformationResponse getWaypointAccidentInformation(AccidentInformationRequest parameters); 
}
