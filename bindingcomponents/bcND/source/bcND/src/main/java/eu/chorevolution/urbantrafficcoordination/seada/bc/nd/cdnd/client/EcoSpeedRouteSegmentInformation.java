
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ecoSpeedRouteSegmentInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ecoSpeedRouteSegmentInformation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="routeSegment" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd}routeSegment"/&gt;
 *         &lt;element name="congestionLevel" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="weatherCondition" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd}weatherCondition"/&gt;
 *         &lt;element name="extraDataWaypoints" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd}extraDataWaypoints"/&gt;
 *         &lt;element name="ecoLevel" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ecoSpeed" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="estimatedEmission" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ecoSpeedRouteSegmentInformation", propOrder = {
    "routeSegment",
    "congestionLevel",
    "weatherCondition",
    "extraDataWaypoints",
    "ecoLevel",
    "ecoSpeed",
    "estimatedEmission"
})
public class EcoSpeedRouteSegmentInformation {

    @XmlElement(required = true)
    protected RouteSegment routeSegment;
    protected int congestionLevel;
    @XmlElement(required = true)
    protected WeatherCondition weatherCondition;
    @XmlElement(required = true)
    protected ExtraDataWaypoints extraDataWaypoints;
    protected int ecoLevel;
    protected int ecoSpeed;
    protected double estimatedEmission;

    /**
     * Gets the value of the routeSegment property.
     * 
     * @return
     *     possible object is
     *     {@link RouteSegment }
     *     
     */
    public RouteSegment getRouteSegment() {
        return routeSegment;
    }

    /**
     * Sets the value of the routeSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteSegment }
     *     
     */
    public void setRouteSegment(RouteSegment value) {
        this.routeSegment = value;
    }

    /**
     * Gets the value of the congestionLevel property.
     * 
     */
    public int getCongestionLevel() {
        return congestionLevel;
    }

    /**
     * Sets the value of the congestionLevel property.
     * 
     */
    public void setCongestionLevel(int value) {
        this.congestionLevel = value;
    }

    /**
     * Gets the value of the weatherCondition property.
     * 
     * @return
     *     possible object is
     *     {@link WeatherCondition }
     *     
     */
    public WeatherCondition getWeatherCondition() {
        return weatherCondition;
    }

    /**
     * Sets the value of the weatherCondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeatherCondition }
     *     
     */
    public void setWeatherCondition(WeatherCondition value) {
        this.weatherCondition = value;
    }

    /**
     * Gets the value of the extraDataWaypoints property.
     * 
     * @return
     *     possible object is
     *     {@link ExtraDataWaypoints }
     *     
     */
    public ExtraDataWaypoints getExtraDataWaypoints() {
        return extraDataWaypoints;
    }

    /**
     * Sets the value of the extraDataWaypoints property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtraDataWaypoints }
     *     
     */
    public void setExtraDataWaypoints(ExtraDataWaypoints value) {
        this.extraDataWaypoints = value;
    }

    /**
     * Gets the value of the ecoLevel property.
     * 
     */
    public int getEcoLevel() {
        return ecoLevel;
    }

    /**
     * Sets the value of the ecoLevel property.
     * 
     */
    public void setEcoLevel(int value) {
        this.ecoLevel = value;
    }

    /**
     * Gets the value of the ecoSpeed property.
     * 
     */
    public int getEcoSpeed() {
        return ecoSpeed;
    }

    /**
     * Sets the value of the ecoSpeed property.
     * 
     */
    public void setEcoSpeed(int value) {
        this.ecoSpeed = value;
    }

    /**
     * Gets the value of the estimatedEmission property.
     * 
     */
    public double getEstimatedEmission() {
        return estimatedEmission;
    }

    /**
     * Sets the value of the estimatedEmission property.
     * 
     */
    public void setEstimatedEmission(double value) {
        this.estimatedEmission = value;
    }

}
