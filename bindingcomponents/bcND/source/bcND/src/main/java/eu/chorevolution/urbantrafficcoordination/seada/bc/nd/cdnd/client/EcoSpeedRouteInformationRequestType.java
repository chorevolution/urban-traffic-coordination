
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ecoSpeedRouteInformationRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ecoSpeedRouteInformationRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="choreographyId" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd}choreographyInstanceRequest"/&gt;
 *         &lt;element name="ecoSpeedRouteInformationRequest" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd}ecoSpeedRouteInformationRequest"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ecoSpeedRouteInformationRequestType", propOrder = {
    "choreographyId",
    "ecoSpeedRouteInformationRequest"
})
public class EcoSpeedRouteInformationRequestType {

    @XmlElement(required = true)
    protected ChoreographyInstanceRequest choreographyId;
    @XmlElement(required = true)
    protected EcoSpeedRouteInformationRequest ecoSpeedRouteInformationRequest;

    /**
     * Gets the value of the choreographyId property.
     * 
     * @return
     *     possible object is
     *     {@link ChoreographyInstanceRequest }
     *     
     */
    public ChoreographyInstanceRequest getChoreographyId() {
        return choreographyId;
    }

    /**
     * Sets the value of the choreographyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChoreographyInstanceRequest }
     *     
     */
    public void setChoreographyId(ChoreographyInstanceRequest value) {
        this.choreographyId = value;
    }

    /**
     * Gets the value of the ecoSpeedRouteInformationRequest property.
     * 
     * @return
     *     possible object is
     *     {@link EcoSpeedRouteInformationRequest }
     *     
     */
    public EcoSpeedRouteInformationRequest getEcoSpeedRouteInformationRequest() {
        return ecoSpeedRouteInformationRequest;
    }

    /**
     * Sets the value of the ecoSpeedRouteInformationRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link EcoSpeedRouteInformationRequest }
     *     
     */
    public void setEcoSpeedRouteInformationRequest(EcoSpeedRouteInformationRequest value) {
        this.ecoSpeedRouteInformationRequest = value;
    }

}
