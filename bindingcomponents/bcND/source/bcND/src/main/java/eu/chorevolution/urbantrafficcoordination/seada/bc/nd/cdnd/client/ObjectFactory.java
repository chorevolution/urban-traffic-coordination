
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EcoRoutesRequestElementRequest_QNAME = new QName("http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd", "ecoRoutesRequestElementRequest");
    private final static QName _EcoRoutesResponseElementResponse_QNAME = new QName("http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd", "ecoRoutesResponseElementResponse");
    private final static QName _EcoRoutesResponseElementRequest_QNAME = new QName("http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd", "ecoRoutesResponseElementRequest");
    private final static QName _EcoSpeedRouteInformationRequestElementRequest_QNAME = new QName("http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd", "ecoSpeedRouteInformationRequestElementRequest");
    private final static QName _EcoSpeedRouteInformationResponseElementResponse_QNAME = new QName("http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd", "ecoSpeedRouteInformationResponseElementResponse");
    private final static QName _EcoSpeedRouteInformationResponseElementRequest_QNAME = new QName("http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd", "ecoSpeedRouteInformationResponseElementRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EcoRoutesRequestType }
     * 
     */
    public EcoRoutesRequestType createEcoRoutesRequestType() {
        return new EcoRoutesRequestType();
    }

    /**
     * Create an instance of {@link EcoRoutesResponse }
     * 
     */
    public EcoRoutesResponse createEcoRoutesResponse() {
        return new EcoRoutesResponse();
    }

    /**
     * Create an instance of {@link EcoRoutesResponseType }
     * 
     */
    public EcoRoutesResponseType createEcoRoutesResponseType() {
        return new EcoRoutesResponseType();
    }

    /**
     * Create an instance of {@link EcoSpeedRouteInformationRequestType }
     * 
     */
    public EcoSpeedRouteInformationRequestType createEcoSpeedRouteInformationRequestType() {
        return new EcoSpeedRouteInformationRequestType();
    }

    /**
     * Create an instance of {@link EcoSpeedRouteInformationResponse }
     * 
     */
    public EcoSpeedRouteInformationResponse createEcoSpeedRouteInformationResponse() {
        return new EcoSpeedRouteInformationResponse();
    }

    /**
     * Create an instance of {@link EcoSpeedRouteInformationResponseType }
     * 
     */
    public EcoSpeedRouteInformationResponseType createEcoSpeedRouteInformationResponseType() {
        return new EcoSpeedRouteInformationResponseType();
    }

    /**
     * Create an instance of {@link ChoreographyInstanceRequest }
     * 
     */
    public ChoreographyInstanceRequest createChoreographyInstanceRequest() {
        return new ChoreographyInstanceRequest();
    }

    /**
     * Create an instance of {@link EcoRoutesRequest }
     * 
     */
    public EcoRoutesRequest createEcoRoutesRequest() {
        return new EcoRoutesRequest();
    }

    /**
     * Create an instance of {@link Waypoint }
     * 
     */
    public Waypoint createWaypoint() {
        return new Waypoint();
    }

    /**
     * Create an instance of {@link EcoSpeedRouteInformation }
     * 
     */
    public EcoSpeedRouteInformation createEcoSpeedRouteInformation() {
        return new EcoSpeedRouteInformation();
    }

    /**
     * Create an instance of {@link EcoSpeedRouteSegmentInformation }
     * 
     */
    public EcoSpeedRouteSegmentInformation createEcoSpeedRouteSegmentInformation() {
        return new EcoSpeedRouteSegmentInformation();
    }

    /**
     * Create an instance of {@link RouteSegment }
     * 
     */
    public RouteSegment createRouteSegment() {
        return new RouteSegment();
    }

    /**
     * Create an instance of {@link WeatherCondition }
     * 
     */
    public WeatherCondition createWeatherCondition() {
        return new WeatherCondition();
    }

    /**
     * Create an instance of {@link ExtraDataWaypoints }
     * 
     */
    public ExtraDataWaypoints createExtraDataWaypoints() {
        return new ExtraDataWaypoints();
    }

    /**
     * Create an instance of {@link ExtraDataWaypoint }
     * 
     */
    public ExtraDataWaypoint createExtraDataWaypoint() {
        return new ExtraDataWaypoint();
    }

    /**
     * Create an instance of {@link EcoSpeedRoutesInformationRequest }
     * 
     */
    public EcoSpeedRoutesInformationRequest createEcoSpeedRoutesInformationRequest() {
        return new EcoSpeedRoutesInformationRequest();
    }

    /**
     * Create an instance of {@link Route }
     * 
     */
    public Route createRoute() {
        return new Route();
    }

    /**
     * Create an instance of {@link WaypointInfo }
     * 
     */
    public WaypointInfo createWaypointInfo() {
        return new WaypointInfo();
    }

    /**
     * Create an instance of {@link EcoSpeedRoutesInformationResponse }
     * 
     */
    public EcoSpeedRoutesInformationResponse createEcoSpeedRoutesInformationResponse() {
        return new EcoSpeedRoutesInformationResponse();
    }

    /**
     * Create an instance of {@link EcoSpeedRoutesInformationResponseType }
     * 
     */
    public EcoSpeedRoutesInformationResponseType createEcoSpeedRoutesInformationResponseType() {
        return new EcoSpeedRoutesInformationResponseType();
    }

    /**
     * Create an instance of {@link EcoSpeedRouteInformationRequest }
     * 
     */
    public EcoSpeedRouteInformationRequest createEcoSpeedRouteInformationRequest() {
        return new EcoSpeedRouteInformationRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EcoRoutesRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd", name = "ecoRoutesRequestElementRequest")
    public JAXBElement<EcoRoutesRequestType> createEcoRoutesRequestElementRequest(EcoRoutesRequestType value) {
        return new JAXBElement<EcoRoutesRequestType>(_EcoRoutesRequestElementRequest_QNAME, EcoRoutesRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EcoRoutesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd", name = "ecoRoutesResponseElementResponse")
    public JAXBElement<EcoRoutesResponse> createEcoRoutesResponseElementResponse(EcoRoutesResponse value) {
        return new JAXBElement<EcoRoutesResponse>(_EcoRoutesResponseElementResponse_QNAME, EcoRoutesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EcoRoutesResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd", name = "ecoRoutesResponseElementRequest")
    public JAXBElement<EcoRoutesResponseType> createEcoRoutesResponseElementRequest(EcoRoutesResponseType value) {
        return new JAXBElement<EcoRoutesResponseType>(_EcoRoutesResponseElementRequest_QNAME, EcoRoutesResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EcoSpeedRouteInformationRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd", name = "ecoSpeedRouteInformationRequestElementRequest")
    public JAXBElement<EcoSpeedRouteInformationRequestType> createEcoSpeedRouteInformationRequestElementRequest(EcoSpeedRouteInformationRequestType value) {
        return new JAXBElement<EcoSpeedRouteInformationRequestType>(_EcoSpeedRouteInformationRequestElementRequest_QNAME, EcoSpeedRouteInformationRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EcoSpeedRouteInformationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd", name = "ecoSpeedRouteInformationResponseElementResponse")
    public JAXBElement<EcoSpeedRouteInformationResponse> createEcoSpeedRouteInformationResponseElementResponse(EcoSpeedRouteInformationResponse value) {
        return new JAXBElement<EcoSpeedRouteInformationResponse>(_EcoSpeedRouteInformationResponseElementResponse_QNAME, EcoSpeedRouteInformationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EcoSpeedRouteInformationResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd", name = "ecoSpeedRouteInformationResponseElementRequest")
    public JAXBElement<EcoSpeedRouteInformationResponseType> createEcoSpeedRouteInformationResponseElementRequest(EcoSpeedRouteInformationResponseType value) {
        return new JAXBElement<EcoSpeedRouteInformationResponseType>(_EcoSpeedRouteInformationResponseElementRequest_QNAME, EcoSpeedRouteInformationResponseType.class, null, value);
    }

}
