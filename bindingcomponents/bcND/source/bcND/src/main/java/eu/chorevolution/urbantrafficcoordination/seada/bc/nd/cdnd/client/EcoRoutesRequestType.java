
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ecoRoutesRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ecoRoutesRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="choreographyId" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd}choreographyInstanceRequest"/&gt;
 *         &lt;element name="ecoRoutesRequest" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd}ecoRoutesRequest"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ecoRoutesRequestType", propOrder = {
    "choreographyId",
    "ecoRoutesRequest"
})
public class EcoRoutesRequestType {

    @XmlElement(required = true)
    protected ChoreographyInstanceRequest choreographyId;
    @XmlElement(required = true)
    protected EcoRoutesRequest ecoRoutesRequest;

    /**
     * Gets the value of the choreographyId property.
     * 
     * @return
     *     possible object is
     *     {@link ChoreographyInstanceRequest }
     *     
     */
    public ChoreographyInstanceRequest getChoreographyId() {
        return choreographyId;
    }

    /**
     * Sets the value of the choreographyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChoreographyInstanceRequest }
     *     
     */
    public void setChoreographyId(ChoreographyInstanceRequest value) {
        this.choreographyId = value;
    }

    /**
     * Gets the value of the ecoRoutesRequest property.
     * 
     * @return
     *     possible object is
     *     {@link EcoRoutesRequest }
     *     
     */
    public EcoRoutesRequest getEcoRoutesRequest() {
        return ecoRoutesRequest;
    }

    /**
     * Sets the value of the ecoRoutesRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link EcoRoutesRequest }
     *     
     */
    public void setEcoRoutesRequest(EcoRoutesRequest value) {
        this.ecoRoutesRequest = value;
    }

}
