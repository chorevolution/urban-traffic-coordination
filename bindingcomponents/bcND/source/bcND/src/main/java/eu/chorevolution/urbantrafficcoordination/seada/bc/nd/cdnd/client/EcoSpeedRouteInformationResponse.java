
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ecoSpeedRouteInformationResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ecoSpeedRouteInformationResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ecoSpeedRouteInformation" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd}ecoSpeedRouteInformation"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ecoSpeedRouteInformationResponse", propOrder = {
    "ecoSpeedRouteInformation"
})
public class EcoSpeedRouteInformationResponse {

    @XmlElement(required = true)
    protected EcoSpeedRouteInformation ecoSpeedRouteInformation;

    /**
     * Gets the value of the ecoSpeedRouteInformation property.
     * 
     * @return
     *     possible object is
     *     {@link EcoSpeedRouteInformation }
     *     
     */
    public EcoSpeedRouteInformation getEcoSpeedRouteInformation() {
        return ecoSpeedRouteInformation;
    }

    /**
     * Sets the value of the ecoSpeedRouteInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link EcoSpeedRouteInformation }
     *     
     */
    public void setEcoSpeedRouteInformation(EcoSpeedRouteInformation value) {
        this.ecoSpeedRouteInformation = value;
    }

}
