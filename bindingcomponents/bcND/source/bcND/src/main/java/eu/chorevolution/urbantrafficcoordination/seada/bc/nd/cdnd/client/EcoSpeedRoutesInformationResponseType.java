
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ecoSpeedRoutesInformationResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ecoSpeedRoutesInformationResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="choreographyId" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd}choreographyInstanceRequest"/&gt;
 *         &lt;element name="ecoSpeedRoutesInformationResponse" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cd/nd}ecoSpeedRoutesInformationResponse"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ecoSpeedRoutesInformationResponseType", propOrder = {
    "choreographyId",
    "ecoSpeedRoutesInformationResponse"
})
public class EcoSpeedRoutesInformationResponseType {

    @XmlElement(required = true)
    protected ChoreographyInstanceRequest choreographyId;
    @XmlElement(required = true)
    protected EcoSpeedRoutesInformationResponse ecoSpeedRoutesInformationResponse;

    /**
     * Gets the value of the choreographyId property.
     * 
     * @return
     *     possible object is
     *     {@link ChoreographyInstanceRequest }
     *     
     */
    public ChoreographyInstanceRequest getChoreographyId() {
        return choreographyId;
    }

    /**
     * Sets the value of the choreographyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChoreographyInstanceRequest }
     *     
     */
    public void setChoreographyId(ChoreographyInstanceRequest value) {
        this.choreographyId = value;
    }

    /**
     * Gets the value of the ecoSpeedRoutesInformationResponse property.
     * 
     * @return
     *     possible object is
     *     {@link EcoSpeedRoutesInformationResponse }
     *     
     */
    public EcoSpeedRoutesInformationResponse getEcoSpeedRoutesInformationResponse() {
        return ecoSpeedRoutesInformationResponse;
    }

    /**
     * Sets the value of the ecoSpeedRoutesInformationResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link EcoSpeedRoutesInformationResponse }
     *     
     */
    public void setEcoSpeedRoutesInformationResponse(EcoSpeedRoutesInformationResponse value) {
        this.ecoSpeedRoutesInformationResponse = value;
    }

}
