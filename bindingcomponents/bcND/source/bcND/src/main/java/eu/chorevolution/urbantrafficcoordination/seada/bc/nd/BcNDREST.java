package eu.chorevolution.urbantrafficcoordination.seada.bc.nd;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.CdNDPT;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.CdNDService;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.ChoreographyInstanceRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.EcoRoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.EcoRoutesRequestType;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.EcoRoutesResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.EcoSpeedRouteInformationRequestType;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.EcoSpeedRouteInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.wrapper.EcoRoutesResponseReturnType;

public class BcNDREST {

	private static Logger logger = LoggerFactory.getLogger(BcNDREST.class);
	
	private static CdNDService cdNDService;
	private static CdNDPT cdNDPT;
	
	static{
		cdNDService = new CdNDService();
		cdNDPT = cdNDService.getCdNDPort();
		Client client = ClientProxy.getClient(cdNDPT);
		HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
		HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
		httpClientPolicy.setConnectionTimeout(0);
		httpClientPolicy.setReceiveTimeout(0);
		httpConduit.setClient(httpClientPolicy);		
	}
	
	@Path("/getEcoRoutes")
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public EcoRoutesResponseReturnType getEcoRoutes(EcoRoutesRequest ecoRoutesRequest){
	
		logger.info("CALLED getEcoRoutes ON BC-ND-REST");
		logger.info("START TIME OPERATION getEcoRoutes ON bcND: "+System.currentTimeMillis());			
		// generate choreography ID
		String choreographyID = String.valueOf(System.nanoTime());
		// create choreography instance request
		ChoreographyInstanceRequest choreographyInstanceRequest = new ChoreographyInstanceRequest();
		choreographyInstanceRequest.setChoreographyId(choreographyID);
		// create ecoRoutesRequestType
		EcoRoutesRequestType ecoRoutesRequestType = new EcoRoutesRequestType();
		ecoRoutesRequestType.setChoreographyId(choreographyInstanceRequest);	
		ecoRoutesRequestType.setEcoRoutesRequest(ecoRoutesRequest);
		// call getEcoRoutes on cdND
		EcoRoutesResponse ecoRoutesResponse = cdNDPT.getEcoRoutes(ecoRoutesRequestType);
		// create ecoRoutesResponseReturnType
		EcoRoutesResponseReturnType ecoRoutesResponseReturnType = new EcoRoutesResponseReturnType();
		ecoRoutesResponseReturnType.setChoreographyId(choreographyInstanceRequest);
		ecoRoutesResponseReturnType.setEcoRoutesResponse(ecoRoutesResponse);
		logger.info("END TIME OPERATION getEcoRoutes ON bcND: "+System.currentTimeMillis());				
		return ecoRoutesResponseReturnType;
	}

	@Path("/getEcoSpeedRouteInformation")	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public EcoSpeedRouteInformationResponse getEcoSpeedRouteInformation(EcoSpeedRouteInformationRequestType ecoSpeedRouteInformationRequestType){
		
		logger.info("CALLED getEcoSpeedRouteInformation ON BC-ND-REST");
		return cdNDPT.getEcoSpeedRouteInformation(ecoSpeedRouteInformationRequestType);
	}
	
}
