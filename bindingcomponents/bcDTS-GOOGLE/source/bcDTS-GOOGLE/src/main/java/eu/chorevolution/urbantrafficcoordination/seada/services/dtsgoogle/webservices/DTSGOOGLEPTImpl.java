package eu.chorevolution.urbantrafficcoordination.seada.services.dtsgoogle.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.DTSGOOGLEPT;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.RoutesSuggestion;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsgoogle.business.BusinessException;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsgoogle.business.DTSGoogleService;

@Component(value = "DTSGOOGLEPTImpl")
public class DTSGOOGLEPTImpl implements DTSGOOGLEPT{

	private static Logger logger = LoggerFactory.getLogger(DTSGOOGLEPTImpl.class);
	
	@Autowired
	private DTSGoogleService service;
	
	@Override
	public RoutesSuggestion routesRequest(RoutesRequest parameters) {
		logger.info("CALLED routeRequest ON DTS-GOOGLE");
		logger.info("START TIME OPERATION routesRequest ON DTS-GOOGLE: "+System.currentTimeMillis());				
		//logger.info(String.valueOf(parameters.getOrigin().getLat()));
		RoutesSuggestion routesSuggestion = null;
		try {
			routesSuggestion = service.getRoutes(parameters);
		} catch (BusinessException e) {
            logger.error("error", e);
    		throw new RuntimeException(e.getMessage());
		}
		logger.info("END TIME OPERATION routesRequest ON DTS-GOOGLE: "+System.currentTimeMillis());			
		return routesSuggestion;
	}

	
}
