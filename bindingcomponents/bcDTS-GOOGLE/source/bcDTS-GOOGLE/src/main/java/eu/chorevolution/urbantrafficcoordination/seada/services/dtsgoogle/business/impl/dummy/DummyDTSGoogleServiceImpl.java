package eu.chorevolution.urbantrafficcoordination.seada.services.dtsgoogle.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.Route;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.RoutesSuggestion;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsgoogle.business.BusinessException;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsgoogle.business.DTSGoogleService;


@Service
public class DummyDTSGoogleServiceImpl implements DTSGoogleService{

	private static Logger logger = LoggerFactory.getLogger(DummyDTSGoogleServiceImpl.class);	
	
	@Override
	public RoutesSuggestion getRoutes(RoutesRequest routesRequest) throws BusinessException {
		
		logger.info("CALLED getRoutes ON DUMMY DTS-GOOGLE");
		RoutesSuggestion routesSuggestion = new RoutesSuggestion();
		Route route = new Route();
		route.setId(1);
		routesSuggestion.getRoutes().add(route);
		return routesSuggestion;
	}



}
