package eu.chorevolution.urbantrafficcoordination.seada.services.dtshere.business.impl.rest;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.Route;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesSuggestion;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.Waypoint;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.WaypointInfo;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtshere.business.BusinessException;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtshere.business.DTSHereService;



@Service
public class DTSHereServiceImpl implements DTSHereService {

	@Value("#{cfgproperties.appid}")
	private String APP_ID; 
	@Value("#{cfgproperties.appcode}")
	private String APP_CODE; 
	@Value("#{cfgproperties.dtshereurl}")
	private String dtshereurl;
	
	private static Logger logger = LoggerFactory.getLogger(DTSHereService.class);

	@Override
	public RoutesSuggestion getRoutes(RoutesRequest routesRequest) throws BusinessException {
		
		logger.info("CALLED getRoutes ON REST DTS-HERE");			
        logger.error("Getting routes (origin: " + routesRequest.getOrigin() + " --> " + routesRequest.getDestination() + ")");

        List<Route> routes = new LinkedList<Route>();

		try
        {    
            //Time to start to query Nokia HERE API!
            URL url = new URL(dtshereurl +
                    "waypoint0=" + routesRequest.getOrigin() + "&" +
                    "waypoint1=" + routesRequest.getDestination() + "&" +
                    "mode=fastest;car;traffic:enabled" + "&" + 
                    "app_id=" + APP_ID + "&" +
                    "app_code=" + APP_CODE + "&" +
                    "departure=now");

            logger.info("URL = " + url);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-length", "0");
            connection.setUseCaches(false);
            connection.setAllowUserInteraction(false);
            connection.connect();
            int status = connection.getResponseCode();                        
            
            logger.info("Status code: " + status);

            //What happened?
            switch (status)
            {
                case 200:
                {
                    //Great -- success -- let's read the respons and build a string which we can parse into route(s)
                    JSONTokener tokener = new JSONTokener(new InputStreamReader(connection.getInputStream(), "UTF-8"));
                    JSONObject jsonRoot = new JSONObject(tokener);
                    
                    JSONObject jsonResponse = jsonRoot.getJSONObject("response");

                    //Convert JSON into our own list of "Routes"
                    JSONArray jsonRoutes = jsonResponse.getJSONArray("route");

                    for (int routeIndex = 0; routeIndex < jsonRoutes.length(); routeIndex++) {
                        JSONObject jsonRoute = jsonRoutes.getJSONObject(routeIndex);
                        JSONArray jsonLegs = jsonRoute.getJSONArray("leg");

                        //Create route object so that we can fill up with new waypoints as we continue parsing the JSON
                        Route route = new Route();
                        int waypointId = 0;
                        
                        //Note; Nokia HERE provide leg(s) with "manouvers", where the start AND end position is represented by a manuouver, 
                        // so no need for special-treatment of start position like we had to do for Google

                        for (int legIndex = 0; legIndex < jsonLegs.length(); legIndex++) {
                            JSONObject jsonLeg = jsonLegs.getJSONObject(legIndex);
                            JSONArray jsonManeuvers = jsonLeg.getJSONArray("maneuver");

                            for (int maneuverIndex = 0; maneuverIndex < jsonManeuvers.length(); maneuverIndex++) {
                                JSONObject jsonManeuver = jsonManeuvers.getJSONObject(maneuverIndex);

                                JSONObject jsonPosition = jsonManeuver.getJSONObject("position");
                                double positionLatitude = jsonPosition.getDouble("latitude");
                                double positionLongitude = jsonPosition.getDouble("longitude");

                                String instruction = jsonManeuver.getString("instruction");

                                //Create waypoint and info-object to wrap up and represent what we've found
                                Waypoint waypoint = new Waypoint();
                                waypoint.setLat(positionLatitude);
                                waypoint.setLon(positionLongitude);

                                WaypointInfo waypointInfo = new WaypointInfo();
                                waypointInfo.setWaypoint(waypoint);
                                waypointInfo.setInstruction(instruction);
                                waypointInfo.setId(waypointId++);

                                //.. and then simply append to the ongoing route
                                route.getWaypoints().add(waypointInfo);
                            }
                        }

                        //Route finished -- no more info, so let's archive the route and proceed
                        routes.add(route);
                    }
                    
                    break;
                }
                
                case 403:
                {
                    logger.info("Forbidden request; most likely invalid/expired API id/code (" + 
                    		"app_id=" + APP_ID + "&" +
                    		"app_code=" + APP_CODE + ")");
                    
                	break;
                }
                
                default:
                {
                    //Opps -- unexpected status-code returned from server...
                	throw new BusinessException("UNEXPECTED STATUS-CODE RETURNED FROM SERVER");
                }
            }
            
            //Close connection -- we're done
            connection.disconnect();
        }
        catch (Exception e) {
        	throw new BusinessException(e.getMessage());
        }
		RoutesSuggestion routesSuggestion = new RoutesSuggestion();
		routesSuggestion.getRoutes().addAll(routes);
		return routesSuggestion;
	}

}
