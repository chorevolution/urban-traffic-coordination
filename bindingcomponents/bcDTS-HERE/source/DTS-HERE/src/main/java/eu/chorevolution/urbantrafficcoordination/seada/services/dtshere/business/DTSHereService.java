package eu.chorevolution.urbantrafficcoordination.seada.services.dtshere.business;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesSuggestion;


public interface DTSHereService {

	RoutesSuggestion getRoutes(RoutesRequest routesRequest) throws BusinessException;
	
}
