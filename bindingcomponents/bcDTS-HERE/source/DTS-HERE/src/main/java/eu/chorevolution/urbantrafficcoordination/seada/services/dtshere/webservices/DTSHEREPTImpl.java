package eu.chorevolution.urbantrafficcoordination.seada.services.dtshere.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.DTSHEREPT;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesSuggestion;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtshere.business.BusinessException;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtshere.business.DTSHereService;

@Component(value = "DTSHEREPTImpl")
public class DTSHEREPTImpl implements DTSHEREPT{
	private static Logger logger = LoggerFactory.getLogger(DTSHEREPTImpl.class);
	
	@Autowired
	DTSHereService service;

	@Override
	public RoutesSuggestion routesRequest(RoutesRequest parameters) {
		logger.info("CALLED routeRequest ON DTS-HERE");	
		logger.info("START TIME OPERATION routesRequest ON DTS-HERE: "+System.currentTimeMillis());			
		//logger.info(String.valueOf(parameters.getOrigin().getLat()));
		RoutesSuggestion routesSuggestion;
		try {
			routesSuggestion = service.getRoutes(parameters);
		} catch (BusinessException e) {
			logger.error("error",e);
			throw new RuntimeException(e.getMessage());
		}
		logger.info("END TIME OPERATION routesRequest ON DTS-HERE: "+System.currentTimeMillis());			
		return routesSuggestion;
	}
}
