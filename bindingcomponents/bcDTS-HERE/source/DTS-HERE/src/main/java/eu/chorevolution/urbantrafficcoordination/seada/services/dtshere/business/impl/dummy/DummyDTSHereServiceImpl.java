package eu.chorevolution.urbantrafficcoordination.seada.services.dtshere.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.Route;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesSuggestion;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtshere.business.BusinessException;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtshere.business.DTSHereService;


@Service
public class DummyDTSHereServiceImpl implements DTSHereService{

	private static Logger logger = LoggerFactory.getLogger(DummyDTSHereServiceImpl.class);	
	
	@Override
	public RoutesSuggestion getRoutes(RoutesRequest routesRequest) throws BusinessException {
	
		logger.info("CALLED getRoutes ON DUMMY DTS-HERE");
		RoutesSuggestion routesSuggestion = new RoutesSuggestion();
		Route route = new Route();
		route.setId(1);
		routesSuggestion.getRoutes().add(route);
		return routesSuggestion;
	}

}
