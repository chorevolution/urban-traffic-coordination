package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.business.impl.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.WeatherInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.WeatherInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.business.DTSWeatherService;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.wrappers.WeatherInformationRequestWrapper;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.wrappers.WeatherInformationResponseWrapper;

@Service
public class RESTDTSWeatherServiceImpl implements DTSWeatherService{

	private static Logger logger = LoggerFactory.getLogger(RESTDTSWeatherServiceImpl.class);
	
	@Value("#{cfgproperties.host}")
	private String host;

	@Value("#{cfgproperties.port}")
	private int port;

	@Value("#{cfgproperties.path}")
	private String path;
	
	@Override
	public WeatherInformationResponse getWaypointWeatherInformation(WeatherInformationRequest weatherInformationRequest) {
		
		logger.info("CALLED getWaypointWeatherInformation ON REST bcDTS-WEATHER");			
		WeatherInformationResponse response = null;		
		try {
			Gson gson = new GsonBuilder().create();			
			HttpClient httpClient = HttpClientBuilder.create().build();
			URI uri = new URIBuilder()
					.setScheme("http")
					.setHost(host)
					.setPort(port)
					.setPath(path)
					.build();
			HttpPost postRequest = new HttpPost(uri);
			WeatherInformationRequestWrapper weatherInformationRequestWrapper = new WeatherInformationRequestWrapper();
			weatherInformationRequestWrapper.setWeatherInformationRequest(weatherInformationRequest);
			StringEntity requestEntity = new StringEntity(gson.toJson(weatherInformationRequestWrapper), ContentType.APPLICATION_JSON);	
			postRequest.setEntity(requestEntity);				
			HttpResponse httpResponse = httpClient.execute(postRequest);
			String json = EntityUtils.toString(httpResponse.getEntity());
			WeatherInformationResponseWrapper weatherInformationResponseWrapper = gson.fromJson(json, WeatherInformationResponseWrapper.class);
			response = weatherInformationResponseWrapper.getWeatherInformationResponse();
		} catch (URISyntaxException | IOException e) {
			logger.error(e.getMessage(),e);
		}
		return response;
	}
}
