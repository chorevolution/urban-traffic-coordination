package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.DTSWEATHERPT;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.WeatherInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.WeatherInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.business.DTSWeatherService;

@Component(value="DTSWEATHERPTImpl")
public class DTSWEATHERPTImpl implements DTSWEATHERPT{

	private static Logger logger = LoggerFactory.getLogger(DTSWEATHERPTImpl.class);
	
	@Autowired
	DTSWeatherService service;
	
	@Override
	public WeatherInformationResponse waypointWeatherInformation(WeatherInformationRequest parameters) {

		logger.info("CALLED waypointWeatherInformation ON bcDTS-WEATHER");
		logger.info("START TIME OPERATION waypointWeatherInformation ON bcDTS-WEATHER: "+System.currentTimeMillis());			
		WeatherInformationResponse weatherInformationResponse = service.getWaypointWeatherInformation(parameters);
		logger.info("END TIME OPERATION waypointWeatherInformation ON bcDTS-WEATHER: "+System.currentTimeMillis());			
		return weatherInformationResponse;
	}
	
}
