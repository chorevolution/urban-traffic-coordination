package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.WeatherInformationResponse;

public class WeatherInformationResponseWrapper {

	private WeatherInformationResponse weatherInformationResponse;

	public WeatherInformationResponse getWeatherInformationResponse() {
		return weatherInformationResponse;
	}

	public void setWeatherInformationResponse(WeatherInformationResponse weatherInformationResponse) {
		this.weatherInformationResponse = weatherInformationResponse;
	}
	
}
