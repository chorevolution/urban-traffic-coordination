package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.WeatherInformationRequest;

public class WeatherInformationRequestWrapper {

	private WeatherInformationRequest weatherInformationRequest;

	public WeatherInformationRequest getWeatherInformationRequest() {
		return weatherInformationRequest;
	}

	public void setWeatherInformationRequest(WeatherInformationRequest weatherInformationRequest) {
		this.weatherInformationRequest = weatherInformationRequest;
	}
	
}
