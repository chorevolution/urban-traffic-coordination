package eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.PollableChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.DTSHEREPT;
import eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.RoutesSuggestion;

@Component(value = "DTSHEREPTImpl")
public class DTSHEREPTImpl implements DTSHEREPT {

	@Autowired
	private ApplicationContext appContext;

	private static Logger logger = LoggerFactory.getLogger(DTSHEREPTImpl.class);

	@Override
	public RoutesSuggestion routesRequest(RoutesRequest parameters) {
		logger.info("Received message from CD, forwarding into channel");
		MessageChannel inputChannel = appContext.getBean("inputChannel", MessageChannel.class);
		Message<RoutesRequest> inputMessage = MessageBuilder.withPayload(parameters).build();
		inputChannel.send(inputMessage);
		PollableChannel outputChannel = appContext.getBean("outputChannel", PollableChannel.class);
		Message<RoutesSuggestion> response = (Message<RoutesSuggestion>) outputChannel.receive();
		logger.info("Found message into channel, sending back");
		return response.getPayload();
	}

}
