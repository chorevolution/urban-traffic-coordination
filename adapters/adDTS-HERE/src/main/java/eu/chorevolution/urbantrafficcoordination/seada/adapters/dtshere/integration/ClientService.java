package eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.integration;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.targetservice.DTSHEREPT;
import eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.targetservice.DTSHEREService;
import eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.targetservice.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.targetservice.RoutesSuggestion;

public class ClientService {

	private static Logger logger = LoggerFactory.getLogger(ClientService.class);

	public Message<RoutesSuggestion> printMessage(Message<RoutesRequest> message) {
		logger.info("Received message");

		DTSHEREService service = new DTSHEREService();
		DTSHEREPT dtsGooglePT = service.getDTSHEREPort();
		Client client = ClientProxy.getClient(dtsGooglePT);
		HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
		HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
		httpClientPolicy.setConnectionTimeout(0);
		httpClientPolicy.setReceiveTimeout(0);
		httpConduit.setClient(httpClientPolicy);

		logger.info("Forwarding request to binding component");

		RoutesSuggestion response = dtsGooglePT.routesRequest(message.getPayload());

		logger.info("Sending response into channel");

		return MessageBuilder.withPayload(response).build();
	}
}
