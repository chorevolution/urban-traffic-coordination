package eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.targetservice.RoutesRequest;

@Component
public class RequestTransformer {

	private static Logger logger = LoggerFactory.getLogger(RequestTransformer.class);

	@Transformer
	public Message<RoutesRequest> transformRequest(Message<eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.RoutesRequest> input) {
		logger.info("Tranformer received message to tranform to BC format");

		RoutesRequest output = new RoutesRequest();
		output.setOrigin(input.getPayload().getOrigin().getLat() + "," + input.getPayload().getOrigin().getLon());
		output.setDestination(input.getPayload().getDestination().getLat() + "," + input.getPayload().getDestination().getLon());

		logger.info("Ended transformation: sending message into channel");

		return MessageBuilder.withPayload(output).build();
	}
}
