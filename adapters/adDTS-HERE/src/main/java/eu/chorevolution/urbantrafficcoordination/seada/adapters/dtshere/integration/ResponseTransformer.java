package eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.Route;
import eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.RoutesSuggestion;
import eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.Waypoint;
import eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.WaypointInfo;


@Component
public class ResponseTransformer {

	private static Logger logger = LoggerFactory.getLogger(ResponseTransformer.class);

	@Transformer
	public Message<RoutesSuggestion> transformResponse(Message<eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.targetservice.RoutesSuggestion> inputMessage) {
		logger.info("Tranformer received message to tranform to CD format");

		eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.targetservice.RoutesSuggestion inputRoutes = inputMessage.getPayload();

		RoutesSuggestion outputRoutes = new RoutesSuggestion();

		// Transform returned object into new object: they should be the same but belong to different domains.
		for (eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.targetservice.Route inputRoute : inputRoutes.getRoutes()) {
			Route outputRoute = new Route();
			outputRoute.setId(inputRoute.getId());

			for (eu.chorevolution.urbantrafficcoordination.seada.adapters.dtshere.targetservice.WaypointInfo inputWaypointInfo : inputRoute.getWaypoints()) {
				WaypointInfo outputWaypointInfo = new WaypointInfo();

				Waypoint outputWaypoint = new Waypoint();
				outputWaypoint.setLat(inputWaypointInfo.getWaypoint().getLat());
				outputWaypoint.setLon(inputWaypointInfo.getWaypoint().getLon());

				outputWaypointInfo.setWaypoint(outputWaypoint);
				outputWaypointInfo.setId(inputWaypointInfo.getId());
				outputWaypointInfo.setInstruction(inputWaypointInfo.getInstruction());
				outputWaypointInfo.setType(inputWaypointInfo.getType());

				outputRoute.getWaypoints().add(outputWaypointInfo);
			}
			outputRoutes.getRoutes().add(outputRoute);
		}

		logger.info("Tranformer ended transformation to CD format, sending into channel");
		return MessageBuilder.withPayload(outputRoutes).build();
	}
}
