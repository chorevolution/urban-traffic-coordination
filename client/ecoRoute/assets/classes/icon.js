var Icon = React.createClass({
    displayName: "Icon",
    render: function() {
        var type = this.props.type;
        var title = this.props.title;
        return (
            React.createElement("svg", { title: title, className: "icon", dangerouslySetInnerHTML: { __html: '<use xlink:href="assets/icons.svg#icon-' + type + '"></use>' }, width: this.props.width, height: this.props.height })
        );
    }
});
export default class Icon {};