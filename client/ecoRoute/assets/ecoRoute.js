var iconPath = 'assets/icons/';
var requestURL = 'http://localhost:3005/getEcoRoutes';
//var requestURL = 'http://localhost:9090/bcND/getEcoRoutes';
//var requestURL = 'http://ec2-34-248-81-237.eu-west-1.compute.amazonaws.com:9090/bcND/getEcoRoutes';
var line = [];
var origin = "origin";
var destination = "destination";
var startRequestTime = 0;
var endRequestTime = 0;
var EcoRoute = {
    directionsService: new google.maps.DirectionsService(),
    directionsRenderer: new google.maps.DirectionsRenderer(),
    elevationService: new google.maps.ElevationService(),
    longestDistance: 0,
    highestElevation: 0,
    map: {},
    alertVisibility: false,
    lowestElevation: Infinity,
    chartWidth: 400,
    chartBarWidth: 2,
    trvRoutes: []
};
//import mockedRoute from './mockdata/route.json';

var App = React.createClass({
    displayName: "App",
    getInitialState: function() {
        return {
            start: '',
            end: '',
            routes: null,
            speed: 0,
            emission: 0,
            distanceUnit: localStorage['ecoroute:distanceUnit'] || 'km',
            heightUnit: localStorage['ecoroute:heightUnit'] || 'm'
        };
    },
    componentDidMount: function() {
        this.hashChange();
        var self = this;
        window.onhashchange = function() {
            self.hashChange();
        };
    },
    componentDidUpdate: function() {
        localStorage['ecoroute:distanceUnit'] = this.state.distanceUnit;
        localStorage['ecoroute:heightUnit'] = this.state.heightUnit;
    },
    hashChange: function() {
        var hash = location.hash.slice(1);
        if (!hash) return;

        var locations = hash.split('/');
        var origin = decodeURIComponent(locations[0]);
        var destination = decodeURIComponent(locations[1]);

        this.setState({
            start: origin,
            end: destination
        });

        this.getRoutes();
    },
    getRoutes: function() {
        var self = this;
        var state = this.state;
        startRequestTime = Date.now();
        origin = state.start;
        destination = state.end;
        EcoRoute.directionsService.route({
            origin: state.start,
            destination: state.end,
            travelMode: google.maps.TravelMode['DRIVING'],
            provideRouteAlternatives: true,
            unitSystem: google.maps.UnitSystem.METRIC
        }, function(response, status) {
            var start_location = response.routes[0].legs[0].start_location;
            var end_location = response.routes[0].legs[response.routes[0].legs.length - 1].end_location;
            fetch(requestURL, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'mode': 'no-cors'
                    },
                    body: JSON.stringify({
                        ecoRoutesRequest: {
                            origin: {
                                lat: start_location.lat(),
                                lon: start_location.lng()
                            },
                            destination: {
                                lat: end_location.lat(),
                                lon: end_location.lng()
                            }
                        }
                    })
                }).then((response) => response.json())
                .then((responseData) => {
                    origin = state.start;
                    destination = state.end;
                    console.log(state.end);
                    EcoRoute.trvRoutes = responseData.displayAndShowSuggestedRoutesMessageResponse;
                    endRequestTime = Date.now();
                    console.log("total time:");
                    console.log(endRequestTime - startRequestTime);
                    //EcoRoute.map.setCenter(new google.maps.LatLng(responseData.ecoRoutesResponseReturnType.ecoRoutesResponse.ecoSpeedRoutesInformation[0].ecoSpeedRouteSegmentsInformation[0].routeSegment.start.lat, responseData.ecoRoutesResponseReturnType.ecoRoutesResponse.ecoSpeedRoutesInformation[0].ecoSpeedRouteSegmentsInformation[0].routeSegment.start.lon));
                        
                    responseData.ecoRoutesResponseReturnType.ecoRoutesResponse.ecoSpeedRoutesInformation.forEach(function(route, i) {
                        route.ecoSpeedRouteSegmentsInformation.forEach(function(segment, j) {
                            var color = self.getColorByCongestionLevelAndRouteIndex(segment.congestionLevel, i);
                            
                            var strokeOpacity = (i == 0 ? 1.0 : 0.3);
                            var poly = new google.maps.Polyline({
                                strokeColor: color,
                                strokeOpacity: strokeOpacity,
                                strokeWeight: 3
                            });
                            var path = poly.getPath();
                            poly.setMap(EcoRoute.map);
                            if (segment.routeSegment.path != undefined) {
                                var decodedPath = google.maps.geometry.encoding.decodePath(segment.routeSegment.path);
                                decodedPath.forEach(function(point) {
                                    path.push(new google.maps.LatLng(point.lat(), point.lng()));
                                });
                            } else {
                                path.push(new google.maps.LatLng(segment.routeSegment.start.lat, segment.routeSegment.start.lon));
                                path.push(new google.maps.LatLng(segment.routeSegment.end.lat, segment.routeSegment.end.lon));
                            }

                            if (segment.extraDataWaypoints.extraDataWaypointsInformation != undefined) {
                                if (Array.isArray(segment.extraDataWaypoints.extraDataWaypointsInformation)) {
                                    segment.extraDataWaypoints.extraDataWaypointsInformation.forEach(function(accident) {
                                        self.addMarker(accident.latitude, accident.longitude, accident.situationType);
                                    });
                                } else {
                                    self.addMarker(segment.extraDataWaypoints.extraDataWaypointsInformation.latitude,
                                        segment.extraDataWaypoints.extraDataWaypointsInformation.longitude,
                                        segment.extraDataWaypoints.extraDataWaypointsInformation.situationType)
                                }

                            }

                            line.push(poly);
                        })
                    });
                });
        });
    },

    getColorByCongestionLevelAndRouteIndex: function(congestionLevel, routeIndex) {
        var routeColors = [
            ['#2e8b57', '#b22222'],
            ['#bce9d0', '#f2baba']
        ];
        if (routeIndex == 0) {
            return routeColors[0][congestionLevel - 1] //congestion levels are 1 and 2, so we substract 1 to get the index
        }
        return routeColors[0][congestionLevel - 1]; //0 in this case as well
    },
    addMarker: function(lat, lng, situationType) {
        var iconMap = {};
        iconMap['ferry'] = iconPath + 'ferry.png';
        iconMap['obstacle'] = iconPath + 'obstacle.png';
        iconMap['accident'] = iconPath + 'accident.png';
        iconMap['restriction'] = iconPath + 'restriction.png';
        iconMap['trafficinfo'] = iconPath + 'trafficinfo.png';
        iconMap['roadworks'] = iconPath + 'roadworks.png';
        iconMap['roadconditions'] = iconPath + 'roadconditions.png';
        iconMap['bridgeopened'] = iconPath + 'bridgeopened_scaled.png';
        iconMap['other'] = iconPath + 'roadworks.png';

        var situationIcon;
        if (iconMap[situationType] != undefined) {
            situationIcon = iconMap[situationType];
        } else {
            situationIcon = iconMap['other'];
        }
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            icon: situationIcon,
            map: EcoRoute.map
        });
    },

    handleRouteClick: function(index) {
        this.state.routes.forEach(function(d, i) {
            d.selected = (index == i);
        });
        this.setState(this.state);
    },
    handleUnitChange: function(units) {
        this.setState(units);
    },

    invertIndex: function(index) {
        return index == 0 ? 1 : 0;
    },
    render: function() {
        var units = {
            distance: this.state.distanceUnit,
            height: this.state.heightUnit
        };
        return (
            React.createElement("div", null,
                React.createElement(Map, null),
                React.createElement("div", { id: "sidebar" },
                    React.createElement("header", null,
                        React.createElement("h1", null, React.createElement(Icon, { type: "car-side", width: "44", height: "44" }), "     SEADA")
                    ),
                    React.createElement("button", { onClick: this.runVehicle, value: "start navigating" }, "start navigating"),
                    React.createElement(RouteForm, { start: this.state.start, end: this.state.end, units: units, onUnitChange: this.handleUnitChange }),
                    React.createElement("div", null,
                        React.createElement("div", { id: "speed", className: "div-speed" },
                            React.createElement("label", { className: "speed-font speed-position" }, "Speed (km/h):"),
                            React.createElement("label", { className: "speed-font speed-position speed-value-position" }, this.state.speed)
                        ),
                        React.createElement("div", { id: "emission", className: "div-emission" },
                            React.createElement("label", { className: "speed-font speed-position" }, "Emission (g/m):"),
                            React.createElement("label", { className: "speed-font speed-position speed-value-position" }, this.state.emission)
                        )
                    ),
                    React.createElement(Alertbox, { id: "alert", visible: this.state.alertVisibility }, {}), {}
                )
            )
        );
    }
});

var Alertbox = React.createClass({
    displayName: "Alertbox",
    render: function() {
        var partial;
        if (this.props.visible) {
            partial = React.createElement("div", { id: "alert", className: "alert alert-danger" },
                React.createElement("label", null, "Bridge opened")
            );
        } else {
            partial = React.createElement("div", null, {});
        }
        return partial;
    }
});

var Icon = React.createClass({
    displayName: "Icon",
    render: function() {
        var type = this.props.type;
        var title = this.props.title;
        return (
            React.createElement("svg", { title: title, className: "icon", dangerouslySetInnerHTML: { __html: '<use xlink:href="assets/icons.svg#icon-' + type + '"></use>' }, width: this.props.width, height: this.props.height })
        );
    }
});

var Map = React.createClass({
    displayName: "Map",
    getDefaultProps: function() {
        return {
            map: {
                center: new google.maps.LatLng(57.707746, 11.933974),
                zoom: 14,
                disableDefaultUI: true
            }
        };
    },
    statics: {
        pinpointMarker: new google.maps.Marker({
            visible: false,
            clickable: false,
            zIndex: 1000
        }),
        showPinpointMarker: function(location) {
            this.pinpointMarker.setPosition(location);
            this.pinpointMarker.setVisible(true);
        },
        hidePinpointMarker: function() {
            this.pinpointMarker.setVisible(false);
        }
    },
    componentDidMount: function() {
        var node = this.getDOMNode();
        var map = new google.maps.Map(node, this.props.map);
        EcoRoute.map = map;
        Map.pinpointMarker.setMap(map);

        EcoRoute.directionsRenderer.setMap(map);
    },
    render: function() {
        return (
            React.createElement("div", { id: "map-canvas" })
        );
    }
});

var RouteForm = React.createClass({
    displayName: "RouteForm",
    updateLocationHash: function(start, end) {
        if (!start) start = this.props.start;
        if (!end) end = this.props.end;
        if (!start || !end) return;
        location.hash = encodeURIComponent(start) + '/' + encodeURIComponent(end);
    },
    handleSubmit: function() {
        var start = this.refs.start.getDOMNode().value.trim();
        var end = this.refs.end.getDOMNode().value.trim();
        this.updateLocationHash(start, end);
    },
    componentDidMount: function() {
        var startNode = this.refs.start.getDOMNode();
        var endNode = this.refs.end.getDOMNode();
        var start = startNode.value.trim();
        var end = endNode.value.trim();

        if (start && end) {
            if (this.props.start) startNode.value = this.props.start;
            if (this.props.end) endNode.value = this.props.end;
        }

        new google.maps.places.Autocomplete(startNode);
        new google.maps.places.Autocomplete(endNode);
    },
    componentWillReceiveProps: function() {
        if (this.props.start) this.refs.start.getDOMNode().value = this.props.start;
        if (this.props.end) this.refs.end.getDOMNode().value = this.props.end;
    },
    handleFlip: function(e) {
        e.preventDefault();
        var start = this.refs.start.getDOMNode().value.trim();
        var end = this.refs.end.getDOMNode().value.trim();
        this.updateLocationHash(null, end, start);
    },
    handleDistanceChange: function() {
        var unit = this.refs.distanceSelect.getDOMNode().value;
        this.props.onUnitChange({
            distanceUnit: unit
        });
    },
    handleHeightChange: function() {
        var unit = this.refs.heightSelect.getDOMNode().value;
        this.props.onUnitChange({
            heightUnit: unit
        });
    },
    render: function() {
        var units = this.props.units;
        return (
            React.createElement("form", { id: "directions-form", onSubmit: this.handleSubmit },
                React.createElement("div", { className: "field-section" },
                    React.createElement("label", { htmlFor: "directions-start" }, "From"),
                    React.createElement("input", { ref: "start", id: "directions-start", placeholder: origin, required: true })
                ),
                React.createElement("div", { className: "field-section" }, null),
                React.createElement("div", { className: "field-section" },
                    React.createElement("label", { htmlFor: "directions-end" }, "To"),
                    React.createElement("input", { ref: "end", id: "directions-end", placeholder: destination, required: true })
                ),
                React.createElement("div", { className: "form-footer" },
                    React.createElement("button", null, "Go")
                )
            )
        );
    }
});

React.renderComponent(
    React.createElement(App, null),
    document.getElementById('app')
);
