package eu.chorevolution.urbantrafficcoordination.seada.ndclient.starters;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.EcoRoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.EcoSpeedRouteInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.EcoSpeedRouteInformationRequestType;
import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.EcoSpeedRouteInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.Route;
import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.Waypoint;
import eu.chorevolution.urbantrafficcoordination.seada.ndclient.starters.wrappers.EcoRoutesRequestTypeWrapper;
import eu.chorevolution.urbantrafficcoordination.seada.ndclient.starters.wrappers.EcoRoutesResponseReturnType;
import eu.chorevolution.urbantrafficcoordination.seada.ndclient.starters.wrappers.EcoSpeedRouteInformationRequestWrapper;

public class BCNDStarter {

	public static void main(String[] args) {
	  
	  EcoRoutesRequest ecoRoutesRequest = new EcoRoutesRequest();
	  Waypoint origin = new Waypoint();
	  origin.setLat(11);
	  origin.setLon(22);
	  Waypoint destination = new Waypoint();
	  destination.setLat(33);
	  destination.setLon(44);	  
	  ecoRoutesRequest.setOrigin(origin);
	  ecoRoutesRequest.setDestination(destination);	 
	  EcoRoutesResponseReturnType ecoRoutesResponseReturnType = invokeGetEcoRoutes(ecoRoutesRequest);
	  System.out.println("CHOREOGRAPHY ID VALUE: "+ecoRoutesResponseReturnType.getChoreographyId().getChoreographyId());
	  EcoSpeedRouteInformationRequest ecoSpeedRouteInformationRequest = new EcoSpeedRouteInformationRequest();
	  Route route = new Route();
	  route.setId(1);		
	  ecoSpeedRouteInformationRequest.setRoute(route);	
	  EcoSpeedRouteInformationRequestType ecoSpeedRouteInformationRequestType = new EcoSpeedRouteInformationRequestType();
	  
	  ecoSpeedRouteInformationRequestType.setChoreographyId(ecoRoutesResponseReturnType.getChoreographyId());
	  ecoSpeedRouteInformationRequestType.setEcoSpeedRouteInformationRequest(ecoSpeedRouteInformationRequest);
	  EcoSpeedRouteInformationResponse ecoSpeedRouteInformationResponse = invokeGetEcoSpeedRouteInformation(ecoSpeedRouteInformationRequestType);
	}
		
	public static EcoRoutesResponseReturnType invokeGetEcoRoutes(EcoRoutesRequest ecoRoutesRequest){
		
		  String bcNDgetEcoRoutesURL = "http://localhost:9090/bcND/getEcoRoutes";
		  RestTemplate restTemplate = new RestTemplate();
		  HttpHeaders headers = new HttpHeaders();
		  headers.setContentType(MediaType.APPLICATION_JSON);		  
		  EcoRoutesRequestTypeWrapper ecoRoutesRequestTypeWrapper = new EcoRoutesRequestTypeWrapper();
		  ecoRoutesRequestTypeWrapper.setEcoRoutesRequest(ecoRoutesRequest);
		  HttpEntity<Object> entity = new HttpEntity<Object>(ecoRoutesRequestTypeWrapper, headers);
		  ResponseEntity<EcoRoutesResponseReturnType> responseEntity = restTemplate.postForEntity(bcNDgetEcoRoutesURL, entity, EcoRoutesResponseReturnType.class);
		  return responseEntity.getBody();
		  //		  return restTemplate.postForObject(bcNDgetEcoRoutesURL, entity, EcoRoutesResponseReturnType.class);
	}
	
	public static EcoSpeedRouteInformationResponse invokeGetEcoSpeedRouteInformation(EcoSpeedRouteInformationRequestType ecoSpeedRouteInformationRequestType){
		
		  String bcNDgetEcoSpeedRouteInformationURL = "http://localhost:9090/bcND/getEcoSpeedRouteInformation";
		  RestTemplate restTemplate = new RestTemplate();
		  HttpHeaders headers = new HttpHeaders();
		  headers.setContentType(MediaType.APPLICATION_JSON);
		  EcoSpeedRouteInformationRequestWrapper ecoSpeedRouteInformationRequestWrapper = new EcoSpeedRouteInformationRequestWrapper();
		  ecoSpeedRouteInformationRequestWrapper.setEcoSpeedRouteInformationRequestType(ecoSpeedRouteInformationRequestType);
		  HttpEntity<Object> entity = new HttpEntity<Object>(ecoSpeedRouteInformationRequestWrapper, headers);
		  return restTemplate.postForObject(bcNDgetEcoSpeedRouteInformationURL, entity, EcoSpeedRouteInformationResponse.class);
	}	
}
