package eu.chorevolution.urbantrafficcoordination.seada.ndclient.starters.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.EcoSpeedRouteInformationRequestType;

public class EcoSpeedRouteInformationRequestWrapper {

	private EcoSpeedRouteInformationRequestType ecoSpeedRouteInformationRequestType;

	public EcoSpeedRouteInformationRequestType getEcoSpeedRouteInformationRequest() {
		return ecoSpeedRouteInformationRequestType;
	}

	public void setEcoSpeedRouteInformationRequestType(EcoSpeedRouteInformationRequestType ecoSpeedRouteInformationRequestType) {
		this.ecoSpeedRouteInformationRequestType = ecoSpeedRouteInformationRequestType;
	}
	
}
