package eu.chorevolution.urbantrafficcoordination.seada.ndclient.starters.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.EcoRoutesRequest;

public class EcoRoutesRequestTypeWrapper {

	private EcoRoutesRequest ecoRoutesRequest;

	public EcoRoutesRequest getEcoRoutesRequest() {
		return ecoRoutesRequest;
	}

	public void setEcoRoutesRequest(EcoRoutesRequest ecoRoutesRequest) {
		this.ecoRoutesRequest = ecoRoutesRequest;
	}
	
	
}
