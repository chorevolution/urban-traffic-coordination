package eu.chorevolution.urbantrafficcoordination.seada.ndclient.starters;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;

import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.CdNDPT;
import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.CdNDService;
import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.ChoreographyInstanceRequest;
import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.EcoRoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.EcoRoutesRequestType;
import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.EcoRoutesResponse;
import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.EcoSpeedRouteInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.EcoSpeedRouteInformationRequestType;
import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.EcoSpeedRouteInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.Route;
import eu.chorevolution.urbantrafficcoordination.seada.client.cdnd.Waypoint;

public class CDNDStarter {
	
	public static void main(String[] args) {
		
		CdNDService cdNDService = new CdNDService();
		CdNDPT cdNDPT = cdNDService.getCdNDPort();
		Client client = ClientProxy.getClient(cdNDPT);
		HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
		HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
		httpClientPolicy.setConnectionTimeout(0);
		httpClientPolicy.setReceiveTimeout(0);
		httpConduit.setClient(httpClientPolicy);			
		// create ecoRoutesRequest
		EcoRoutesRequest ecoRoutesRequest = new EcoRoutesRequest();
		Waypoint origin = new Waypoint();
		origin.setLat(11);
		origin.setLon(22);
		Waypoint destination = new Waypoint();
		destination.setLat(33);
		destination.setLon(44);	  
		ecoRoutesRequest.setOrigin(origin);
		ecoRoutesRequest.setDestination(destination);
		// generate choreography ID
		String choreographyID = String.valueOf(System.nanoTime());
		// create choreography instance request
		ChoreographyInstanceRequest choreographyInstanceRequest = new ChoreographyInstanceRequest();
		choreographyInstanceRequest.setChoreographyId(choreographyID);
		// create ecoRoutesRequestType
		EcoRoutesRequestType ecoRoutesRequestType = new EcoRoutesRequestType();
		ecoRoutesRequestType.setChoreographyId(choreographyInstanceRequest);
		ecoRoutesRequestType.setEcoRoutesRequest(ecoRoutesRequest);	
		// call getEcoRoutes on cdND
		EcoRoutesResponse ecoRoutesResponse = cdNDPT.getEcoRoutes(ecoRoutesRequestType);
		// create ecoSpeedRouteInformationRequest
		EcoSpeedRouteInformationRequest ecoSpeedRouteInformationRequest = new EcoSpeedRouteInformationRequest();
		Route route = new Route();
		route.setId(1);	
		ecoSpeedRouteInformationRequest.setRoute(route);
		// create ecoSpeedRouteInformationRequestType
		EcoSpeedRouteInformationRequestType ecoSpeedRouteInformationRequestType = new EcoSpeedRouteInformationRequestType();
		ecoSpeedRouteInformationRequestType.setChoreographyId(choreographyInstanceRequest);
		ecoSpeedRouteInformationRequestType.setEcoSpeedRouteInformationRequest(ecoSpeedRouteInformationRequest);
		// create ecoSpeedRouteInformationResponse
		EcoSpeedRouteInformationResponse ecoSpeedRouteInformationResponse = cdNDPT.getEcoSpeedRouteInformation(ecoSpeedRouteInformationRequestType);
	}
	
}
